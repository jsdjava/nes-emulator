package nesengine.sound;

public class Tone {

    /**
     * Enumerated object that defines the possible wave types that can be
     * generated. An indeterminate wave type, at this time, means an illegal
     * tone type, and will result in a null return for any
     * <code>compileTone</code> calls.
     */
    public static enum WaveType {

        SINE,
        TRIANGLE,
        SQUARE,
        INDETERMINATE;
    }
    private double Hz = 0;
    private WaveType toneWaveType;

    /**
     * Constructor for a
     * <code>Tone</code> object.
     *
     * @param Hz Frequency of the tone
     * @param toneWaveType Wave type of the tone
     */
    public Tone(double Hz, WaveType toneWaveType) {
        this.Hz = Hz;
        this.toneWaveType = toneWaveType;
    }

    /**
     * Constructor for a tone object. Similar to its counterpart, the
     * <code>Tone</code> constructor takes in a frequency count, but also takes
     * in a numerical representation of a wave type, with valid types ranging
     * from 0 through 2. If a wave type that is not valid is passed in, the tone
     * is assigned an "indeterminate" form, which means that no tones can be
     * generated with this type.
     *
     * @param Hz Frequency of the tone
     * @param nWaveType Wave type of the tone
     */
    public Tone(double Hz, int nWaveType) {
        this.Hz = Hz;
        if (nWaveType == WaveType.SINE.ordinal()) {
            this.toneWaveType = WaveType.SINE;
        } else if (nWaveType == WaveType.TRIANGLE.ordinal()) {
            this.toneWaveType = WaveType.TRIANGLE;
        } else if (nWaveType == WaveType.SQUARE.ordinal()) {
            this.toneWaveType = WaveType.SQUARE;
        } else {
            this.toneWaveType = WaveType.INDETERMINATE;
        }
    }

    /**
     * Method that gets the frequency of the defined tone.
     *
     * @return The frequency of the tone
     */
    public double getFrequency() {
        return Hz;
    }

    /**
     * Method that gets the set wave type of the defined tone.
     *
     * @return The wave type of the tone
     */
    public WaveType getWavetype() {
        return toneWaveType;
    }

    /**
     * This method will take the
     * <code>Tone</code> object's set frequency, the set wave type, the given
     * millisecond count, and the given volume, and will build a waveform with
     * those parameters.
     *
     * @param ms Time, in milliseconds, of the wave
     * @param volume The volume of the wave, from 1.0 to 0.0
     * @return The <code>Waveform</code>
     */
    public Waveform compileTone(int ms, double volume) {
        if (toneWaveType == WaveType.SINE) {
            return buildSineWave(ms, volume);
        } else if (toneWaveType == WaveType.TRIANGLE) {
            return buildTriangleWave(ms, volume);
        } else if (toneWaveType == WaveType.SQUARE) {
            return buildSquareWave(ms, volume);
        }
        return null;
    }

    private Waveform buildSineWave(int ms, double volume) {
        if (volume > 1.0 || volume < 0.0) {
            volume = 1.0;
        }

        int nCaptures = (int) ((double) ms / 1000 * Waveform.SAMPLE_RATE);
        byte[] bSound = new byte[nCaptures];

        for (int i = 0; i < bSound.length; i++) {
            double period = (double) (Waveform.SAMPLE_RATE / Hz); //Period = time / frequency
            double angle = 2.0 * Math.PI * i / period; //Angle = 2PIx / period
            bSound[i] = (byte) (Math.sin(angle) * Waveform.MAX_WAVE_AMPLITUDE * volume);
        }
        bSound[bSound.length - 1] = 0;
        return new Waveform(bSound);
    }

    private Waveform buildTriangleWave(int ms, double volume) {
        if (volume > 1.0 || volume < 0.0) {
            volume = 1.0;
        }
        int nCaptures = (int) ((double) ms / 1000 * Waveform.SAMPLE_RATE);
        byte[] bSound = new byte[nCaptures];

        for (int i = 0; i < bSound.length; i++) {
            double period = (double) (Waveform.SAMPLE_RATE / Hz);
            double amplitude = (127 * volume);
            //bSound[i] = (byte) ((2 * amplitude / Math.PI) * (Math.asin(Math.sin(2 * Math.PI / period * i)))); //Kept in for comparative reasons
            bSound[i] = (byte) (2 * amplitude / Math.PI);
            bSound[i] = (byte) ((bSound[i]) * Math.asin(Math.sin(2 * Math.PI / period * i)));
        }
        return new Waveform(bSound);
    }

    private Waveform buildSquareWave(int ms, double volume) {
        if (volume > 1.0 || volume < 0.0) {
            volume = 1.0;
        }
        int nCaptures = (int) ((double) ms / 1000 * Waveform.SAMPLE_RATE);
        byte[] bSound = buildSineWave(ms, volume).getByteArray();
        for (int i = 0; i < bSound.length; i++) {
            if (bSound[i] > 0) {
                bSound[i] = (byte) (volume * Waveform.MAX_WAVE_AMPLITUDE);
            } else if (bSound[i] < 0) {
                bSound[i] = (byte) (volume * -Waveform.MAX_WAVE_AMPLITUDE);
            }
        }
        return new Waveform(bSound);
    }
}