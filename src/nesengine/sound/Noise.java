package nesengine.sound;

import java.math.BigInteger;
import java.util.Random;
import java.text.NumberFormat;

public class Noise {

    /**
     * Enumerated object that defines the types of generic noises that can be
     * produced.
     * <code>WHITE</code> refers to randomized white noise, whereas
     * <code>NES</code> refers to an NES-style melodic white noise.
     */
    public static enum NoiseType {

        WHITE,
        NES;
    }
    /**
     * Defines the basic string used at the "startup", which is when the Noise
     * class is first used in a program.
     */
    public static final String loadupStr = "0000000000000001";
    private double Hz = 0;
    private NoiseType nType = null;
    private static String noiseStr = null;
    private static BigInteger cyclicNoise = new BigInteger(loadupStr, 2);
    private static NumberFormat nFormat = NumberFormat.getInstance();

    public Noise(double Hz, NoiseType nType) {
        this.Hz = Hz;
        this.nType = nType;
    }

    /**
     * Generates a generic noise with the set noise frequencies and types.
     *
     * @param ms Time, in milliseconds, of the produced noise
     * @param volume Relative amplitude of the noise
     * @return The <code>Waveform</code> generated with the given and set
     * parameters
     */
    public Waveform generateNoise(int ms, double volume) {

        byte[] bSound = null;

        if (nType == NoiseType.WHITE) {
            bSound = generateWhiteNoise(ms, volume);
        } else if (nType == NoiseType.NES) {
            bSound = generateNESNoise(ms, volume);
        }
        return new Waveform(bSound);
    }

    private byte[] generateWhiteNoise(int ms, double volume) {
        if (volume > 1.0 || volume < 0.0) {
            volume = 1.0;
        }
        Random rand = new Random();

        int nCaptures = (int) (((double) ms) / 1000 * Waveform.SAMPLE_RATE);
        byte[] bSound = new byte[nCaptures];

        rand.nextBytes(bSound);

        for (int i = 0; i < bSound.length; i++) {
            bSound[i] = (byte) (bSound[i] * volume);
        }
        return bSound;
    }

    private byte[] generateNESNoise(int ms, double volume) {
        if (volume > 1.0 || volume < 0.0) {
            volume = 1.0;
        }
        int nCaptures = (int) (((double) ms) / 1000 * Waveform.SAMPLE_RATE);
        byte[] bSound = new byte[nCaptures];

        int j = 3;
        String str = null;
        for (int i = 0; i < bSound.length; i++) {
            str = cyclicNoise.toString(2);
            while (str.length() < loadupStr.length()) {
                str = ("0" + str);
            }
            bSound[i] = (byte) (Integer.parseInt("" + str.charAt(j)) * (Waveform.MAX_WAVE_AMPLITUDE * volume));
            if (--j < 0) {
                j = 3;
                generateNextBitSet();
                for (int k = 0; k < Hz && i < bSound.length; k++) {
                    bSound[i] = 0;
                    i++;
                }
            }
        }

        return bSound;
    }

    private void generateNextBitSet() {
        String bStr = cyclicNoise.toString(2);
        while (bStr.length() < loadupStr.length()) {
            bStr = ("0" + bStr);
        }
        if (bStr.charAt(8) == '1') {
            if (bStr.charAt(15) == '1' && bStr.charAt(9) == '1') {
                cyclicNoise = cyclicNoise.clearBit(15);
            } else if (bStr.charAt(15) == '1' || bStr.charAt(9) == '1') {
                cyclicNoise = cyclicNoise.setBit(15);
            }
        } else {
            if (bStr.charAt(15) == '1' && bStr.charAt(14) == '1') {
                cyclicNoise = cyclicNoise.clearBit(15);
            } else if (bStr.charAt(15) == '1' || bStr.charAt(14) == '1') {
                cyclicNoise = cyclicNoise.setBit(15);
            }
        }
        cyclicNoise = cyclicNoise.shiftRight(1);
    }
}
