package nesengine.sound;

import java.util.ArrayList;
import javax.sound.sampled.AudioFormat;

public class Sound {

    private ArrayList<SoundChannel> channelList;

    public Sound(int channelCount) {
        AudioFormat audFormat = new AudioFormat(Waveform.SAMPLE_RATE, 8, 1, true, true);
        channelList = new ArrayList();
        for (int i = 0; i < channelCount; i++) {
            channelList.add(new SoundChannel(audFormat, "" + i));
        }
    }
    
    public Sound(String[] strChannelNames) {
        AudioFormat audFormat = new AudioFormat(Waveform.SAMPLE_RATE, 8, 1, true, true);
        channelList = new ArrayList();
        for (int i = 0; i < strChannelNames.length; i++) {
            channelList.add(new SoundChannel(audFormat, strChannelNames[i]));
        }
    }
}
