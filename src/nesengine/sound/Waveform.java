package nesengine.sound;

public class Waveform {
    /**
     * Defines the standard sample rate for a generic waveform.
     */
    public static final int SAMPLE_RATE = 16 * 1024;
    /**
     * Maximum wave amplitude. This generally applies only to the square wave,
     * but can be used for other waves that may need a standard max values for
     * byte-type waves.
     */
    public static final int MAX_WAVE_AMPLITUDE = 127;
    
    private final byte[] bSound;
    
    public Waveform(byte[] bSound) {
        this.bSound = bSound;
    }
    
    /**
     * Static method that combines two supplied waveforms, overlaying one upon
     * the other. This reduces the wave precision with each successive call for
     * a given
     * <code>byte</code> waveform, so chaining the method to overlay multiple
     * waveforms is not suggested.
     *
     * @param aSound The waveform to be combined with
     * @param bSound The waveform that will be added to the other
     * @return The combined/mixed waveform
     */
    public static byte[] overlayWaveforms(byte[] aSound, byte[] bSound) {
        int minLength = Math.min(aSound.length, bSound.length);
        byte[] rSound = new byte[minLength];

        for (int i = 0; i < minLength; i++) {
            rSound[i] = (byte) ((aSound[i] / 2) + (bSound[i] / 2));
        }
        return rSound;
    }
    
    /**
     * Static method that concatenates two supplied waveforms. This is very
     * useful for smoothly transitioning between two waveforms to play, rather
     * than attempting to write the two waveforms to the same sound channel.
     *
     * @param aSound Front waveform
     * @param bSound Appended waveform
     * @return The concatenated waveforms
     */
    public static byte[] concatWaveforms(byte[] aSound, byte[] bSound) {
        int nLength = (aSound.length + bSound.length);
        byte[] rSound = new byte[nLength];

        int i = 0;
        for (int j = 0; i < rSound.length && j < aSound.length; i++, j++) {
            rSound[i] = aSound[j];
        }
        for (int j = 0; i < rSound.length && j < bSound.length; i++, j++) {
            rSound[i] = bSound[j];
        }

        return rSound;
    }
    
    /**
     * Method that returns a clone of the stored <code>byte</code> array.
     * Because it is a clone, any manipulation can be done on the array without
     * changing the array stored within the <code>Waveform</code> object.
     * 
     * @return The <code>Waveform</code> object's byte wave
     */
    public byte[] getByteArray() {
        return bSound;
    }
    
    /**
     * Returns the length of the waveform byte array as an integer.
     * 
     * @return The length of the waveform
     */
    public int getWaveformLength() {
        return bSound.length;
    }
}
