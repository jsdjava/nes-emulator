package nesengine.sound;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import javax.sound.sampled.*;

public class SoundChannel {

    /*Waveform List*/
    private volatile ArrayList<Waveform> waveList;
    /*Sound Handling*/
    private SourceDataLine sourceChannel;
    private AudioFormat audFormat;
    private Semaphore queueSemaphore;
    /*Logging*/
    private static final Logger channelLog = Logger.getLogger(SoundChannel.class.getName());
    /*Channel Specific*/
    private String alias = "";

    public SoundChannel(AudioFormat audFormat) {
        this(audFormat, "anon");
    }

    public SoundChannel(AudioFormat audFormat, String alias) {
        this.audFormat = audFormat;
        try {
            sourceChannel = AudioSystem.getSourceDataLine(this.audFormat);
        } catch (LineUnavailableException e) {
            channelLog.log(Level.SEVERE, "Channel: {0} unable to build line!", this.alias);
            System.exit(-1);
        }
        sourceChannel.start();
        beginOutputThread();
    }

    /**
     * Method that writes the supplied
     * <code>Waveform</code> to the channel's output.
     *
     * @param wForm The <code>Waveform</code> to be written to the output
     */
    public synchronized void write(Waveform wForm) {
        synchronized(waveList) {
            waveList.add(wForm);
        }
    }

    /**
     * Method that returns whether or not the channel has any
     * <code>Waveform</code> objects currently queued in its list.
     *
     * @return <code>true</code> if there are any objects waiting to be written
     */
    public synchronized boolean hasQueuedSound() {
        boolean queueStatus = false;
        synchronized(waveList) {
            queueStatus = (!waveList.isEmpty());
        }
        return queueStatus;
    }
    
    public String getAlias() {
        return alias;
    }
    
    @Override
    public String toString() {
        return ("alias: " + alias + "; hasQueuedSound: " + (hasQueuedSound() ? "true" : "false"));
    }

    private void beginOutputThread() {
        Thread t;
        t = new Thread() {
            @Override
            public void run() {
                Waveform currWave;
                while (true) {
                    if (hasQueuedSound()) {
                        synchronized(waveList) {
                            currWave = waveList.remove(0);
                            sourceChannel.write(currWave.getByteArray(), 0, 0);
                        }
                    }
                }
            }
        };
        t.start();
    }
}
