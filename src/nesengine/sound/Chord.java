package nesengine.sound;

import java.util.ArrayList;

public class Chord {

    private ArrayList<Tone> toneList;
    private Tone toneCombo;
    /**
     * Maximum amount of tones that can be combined within the chord.
     */
    public static final int MAX_TONE = 4;

    public Chord(ArrayList<Tone> tList) {
        toneList = tList;
        for (int i = MAX_TONE; toneList.size() > MAX_TONE;) {
            toneList.remove(i);
        }
    }

    /**
     * Combines all of the stored tones, returning a compiled combination of
     * each. With each new tone, precision of the chord decreases, so the total
     * amount of possible tones that can be compiled has been capped at 4.
     *
     * @param ms The length, in milliseconds, of the chord
     * @param volume The amplitude of the chord, from 0.0 to 1.0
     * @return The <code>Waveform</code> combination of all of the compiled
     * tones
     */
    public Waveform compileChord(int ms, double volume) {
        if (volume > 1.0) {
            volume = 1.0;
        }

        Tone tTone;
        byte[] tByte;
        byte[] bSound = null;
        for (int i = 0; i < toneList.size(); i++) {
            tTone = toneList.get(i);
            tByte = tTone.compileTone(ms, volume).getByteArray();
            if (i == 0) {
                bSound = new byte[tByte.length];
            }
            for (int j = 0; j < tByte.length; j++) {
                tByte[j] = (byte) (tByte[j] / toneList.size());
                bSound[j] += (tByte[j] * volume);
            }
        }
        return new Waveform(bSound);
    }
}
