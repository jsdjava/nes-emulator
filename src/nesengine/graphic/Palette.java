package nesengine.graphic;

import java.util.ArrayList;

public class Palette {

    private final ArrayList<Tile> tilePalette;

    /**
     * Basic constructor for a <code>Color</code>-based <code>Palette</color>.
     */
    public Palette() {
        tilePalette = new ArrayList();
    }

    /**
     * Adds a <code>Tile</code> object to the list of usable
     * <code>Tile</code>s.
     * 
     * @param newTile The <code>Tile</code> to be added
     * @return <code>true</code> if the <code>Tile</code> was successfully added
     */
    public boolean addTile(Tile newTile) {
        return tilePalette.add(newTile);
    }
    
    /**
     * Replaces a <code>Tile</code> at a specified index with the new supplied
     * <code>Tile</code>.
     * 
     * @param index The index of the <code>Tile</code> to be replaced
     * @param tTile The new <code>Tile</code> to be used
     */
    public void setTile(int index, Tile tTile) {
        tilePalette.set(index, tTile);
    }

    /**
     * Returns the current number of stored <code>Tile</code> objects within the
     * <code>Palette</code>.
     * 
     * @return The size of the <code>Palette</code>
     */
    public int size() {
        return tilePalette.size();
    }

    /**
     * Returns the <code>Tile</code> stored at the specified index.
     * 
     * @param index The index of the <code>Tile</code> to be returned
     * @return The requested <code>Tile</code>
     */
    public Tile getTile(int index) {
        return tilePalette.get(index);
    }
}