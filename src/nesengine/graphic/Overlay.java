package nesengine.graphic;

import java.awt.Color;

public class Overlay {

    /**
     * The maximum/minimum amount of colors that an
     * <code>Overlay</code> can have.
     */
    public static final int MAX_COLOR_AMOUNT = 16;
    private Color[] colorList;

    /**
     * Standard constructor for the Overlay class.
     * @param colorList 
     */
    public Overlay(Color[] colorList) {
        if (colorList.length == MAX_COLOR_AMOUNT) {
            this.colorList = colorList;
        } else if (colorList.length > MAX_COLOR_AMOUNT) {
            this.colorList = new Color[MAX_COLOR_AMOUNT];
            for (int i = 0; i < MAX_COLOR_AMOUNT; i++) {
                this.colorList[i] = colorList[i];
            }
        } else {
            colorList = new Color[MAX_COLOR_AMOUNT];
            for (int i = 0; i < colorList.length; i++) {
                colorList[i] = Tile.ERR_COLOR;
            }
        }
    }

    /**
     * Returns the
     * <code>Color</code> at the selected index. The method will only return a
     * <code>Color</code> object if the index is valid; otherwise, the method
     * will return
     * <code>null</code>.
     *
     * @param index Index of the <code>Color</code> wanted
     * @return The <code>Color</code> from the specified index
     */
    public Color getColor(int index) {
        return colorList[index];
    }

    /**
     * Overwrites the
     * <code>Color</code> object at the specified index with the new one.
     *
     * @param index Index of the <code>Color</code> object to overwrite
     * @param color New <code>Color</code> object to be stored
     */
    public void setColor(int index, Color color) {
        colorList[index] = color;
    }
}
