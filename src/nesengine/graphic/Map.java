package nesengine.graphic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Map {

    /**
     * Standard width of the screen, in
     * <code>Tile</code>s.
     */
    public static final int TILE_SCREEN_WIDTH = 32;
    /**
     * Standard height of the screen, in
     * <code>Tile</code>s.
     */
    public static final int TILE_SCREEN_HEIGHT = 30;
    private int[][] alphaTileMap;
    private int[][] alphaOverlayMap;
    private int[][] betaTileMap;
    private int[][] betaOverlayMap;
    private int nXLoc = 0;
    private int nYLoc = 0;
    private Sprite[] spriteArray;
    private Overlay tileOverlay;
    private Overlay spriteOverlay;
    private Palette tilePalette;
    private Palette spritePalette;

    /**
     * Method supplied by the
     * <code>Map</code> class to set all array indexes in a 2D array to 0,
     * thereby removing all garbage or previous data.
     *
     * @param array The array to be wiped
     */
    public static void wipe2DIntArray(int[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[j][i] = 0;
            }
        }
    }

    /**
     * Standard constructor for a
     * <code>Map</code> object.
     *
     * @param tilePalette   The <code>Tile</code> palette to be used to render
     *                      with
     * @param spritePalette The <code>Sprite</code> palette to be used to render
     *                      with
     * @param tileOverlay   The <code>Overlay</code> colorset to be used for tiles
     * @param spriteOverlay The <code>Overlay</code> colorset to be sued for
     *                      sprites
     */
    public Map(Palette tilePalette, Palette spritePalette, Overlay tileOverlay, Overlay spriteOverlay) {
        this.tilePalette = tilePalette;
        this.spritePalette = spritePalette;
        this.tileOverlay = tileOverlay;
        this.spriteOverlay = spriteOverlay;
        this.alphaTileMap = new int[32][30];
        this.alphaOverlayMap = new int[8][7];
        wipe2DIntArray(alphaOverlayMap);
        this.betaTileMap = new int[32][30];
        this.betaOverlayMap = new int[8][7];
        wipe2DIntArray(betaOverlayMap);
        spriteArray = new Sprite[64];
    }

    /**
     * Swaps the two arrays that contain the references for all of the rendered
     * <code>Tile</code> objects. Essentially, this swaps what array the screen
     * is moving to.
     */
    public void swapRenderedScreen() {
        int[][] tTileMap = alphaTileMap;
        int[][] tOverlayMap = alphaOverlayMap;

        alphaTileMap = betaTileMap;
        alphaOverlayMap = betaOverlayMap;

        betaTileMap = tTileMap;
        betaOverlayMap = tTileMap;
    }

    /**
     * Writes the specified 2D array over
     * <code>alphaTileMap</code>, setting what tiles the camera is moving from.
     *
     * @param alphaTileMap    New <code>Tile</code> map to write to screen 1
     * @param alphaOverlayMap New <code>Overlay</code> map to write to screen 1
     */
    public void writeAlphaScreen(int[][] alphaTileMap, int[][] alphaOverlayMap) {
        this.alphaTileMap = alphaTileMap;
        this.alphaOverlayMap = alphaOverlayMap;
    }

    /**
     * Writes the specified 2D array over
     * <code>betaTileMap</code>, setting what tiles the camera is moving from.
     *
     * @param betaTileMap    New <code>Tile</code> map to write to screen 2
     * @param betaOverlayMap New <code>Overlay</code> map to write to screen 2
     */
    public void writeBetaScreen(int[][] betaTileMap, int[][] betaOverlayMap) {
        this.betaTileMap = betaTileMap;
        this.betaOverlayMap = betaOverlayMap;
    }

    /**
     * Returns the
     * <code>Palette</code> that stores the selection of
     * <code>Tile</code> objects used by the
     * <code>Sprite</code> class.
     *
     * @return The <code>Palette</code> of <code>Tile</code> objects
     */
    public Palette getSpritePalette() {
        return spritePalette;
    }

    /**
     * Overwrites the
     * <code>Sprite</code> object at the specified index with the supplied
     * <code>Sprite</code> object.
     *
     * @param sprite The new <code>Sprite</code> object
     * @param index  The index of the array to replace
     */
    public void setSprite(Sprite sprite, int index) {
        spriteArray[index] = sprite;
    }

    /**
     * Sets the
     * <code>Tile</code> index reference at the indicated location to the
     * supplied value.
     *
     * @param index  Index of the tile on the 2 screen name tables
     * @param nValue New value for that <code>int</code> reference
     */
    public void setSpot(int index, int nValue) {
        int[][] tileArray = null;
        int relativeIndex = (index - (TILE_SCREEN_WIDTH * TILE_SCREEN_HEIGHT));
        if (relativeIndex < 0) {
            relativeIndex = index;
            tileArray = alphaTileMap;
        } else {
            tileArray = betaTileMap;
        }
        int x = (relativeIndex % TILE_SCREEN_WIDTH);
        int y = (relativeIndex / TILE_SCREEN_WIDTH);

        tileArray[x][y] = nValue;
    }

    /**
     * Sets the <code>Overlya</code> index reference at the indicated location
     * to the supplied value.
     * 
     * @param index The index of the overlay on the 2 screen name tables
     * @param nValue New value for that <code>int</code> reference
     */
    public void setOverlaySpot(int index, int nValue) {
        int[][] tileArray = null;
        int relativeIndex = (index - (TILE_SCREEN_WIDTH * TILE_SCREEN_HEIGHT));
        if (relativeIndex < 0) {
            relativeIndex = index;
            tileArray = alphaOverlayMap;
        } else {
            tileArray = betaOverlayMap;
        }
        int x = (relativeIndex % TILE_SCREEN_WIDTH);
        int y = (relativeIndex / TILE_SCREEN_WIDTH);

        tileArray[x][y] = nValue;
    }
    
    /**
     * Renders the next sequential tile location to the supplied
     * <code>BufferedImage</code>, utilizing the supplied x and y offsets. As
     * the offsets increase, the program selects a different tile from the
     * <code>Tile</code> array to render at that spot, and then moves
     * the tile over 0-7 pixels, depending on the leftover offset value.
     * 
     * @param screenImg The <code>BufferedImage</code> to render to
     * @param offsetX The x index offset
     * @param offsetY The y index offset
     */
    public void renderNextTile(BufferedImage screenImg, int offsetX, int offsetY) {

        int[][] currTileMap;
        int[][] currOverlayMap;
        Tile currTile;
        int index = 0;
        int nXOffset = (offsetX / Tile.PIXELS_PER_TILE);
        if ((nXLoc + nXOffset) >= alphaTileMap.length) {
            currTileMap = betaTileMap;
            currOverlayMap = betaOverlayMap;
            index = (nXLoc + nXOffset) - alphaTileMap.length;
            if (index >= betaTileMap.length) {
                currTileMap = alphaTileMap;
                currOverlayMap = alphaOverlayMap;
                index -= betaTileMap.length;
            }
        } else {
            currTileMap = alphaTileMap;
            currOverlayMap = alphaOverlayMap;
            index = (nXLoc + nXOffset);
        }
        currTile = tilePalette.getTile(currTileMap[index][nYLoc]);
        Color[] curLookup = getCurLookup(currOverlayMap[index][nYLoc], tileOverlay);
        currTile.render(screenImg, ((nXLoc * Tile.PIXELS_PER_TILE) - (offsetX % 8)), (nYLoc * Tile.PIXELS_PER_TILE), curLookup,false);
        incNextTile();
    }

    /**
     * Increments the sequential
     * <code>Tile</code> rendering to the next index. Use of this outside of the
     * <code>Map</code> class allows for skipping of certain tiles during
     * rendering.
     */
    public void incNextTile() {
        nXLoc++;
        if (nXLoc >= alphaTileMap.length) {
            nXLoc = 0;
            nYLoc++;
        }
        if (nYLoc >= alphaTileMap[0].length) {
            nYLoc = 0;
        }
    }

    /**
     * Resets the next render location to 0, 0. This has the effect of returning
     * the "cursor" back to the first tile in the array.
     */
    public void resetTileScan() {
        nXLoc = 0;
        nYLoc = 0;
    }
    
    /**
     * Renders the <code>Sprite</code> array to the supplied
     * <code>BufferedImage</code>.
     * @param img The <code>BufferedImage</code> to write to
     */
    public void renderSpriteList(BufferedImage img) {
        for (int i = 0; i < spriteArray.length; i++) {
            if (spriteArray[i] != null) {
                Sprite sprite = spriteArray[i];
                int[] requiredTiles = sprite.getRequiredTiles();
                Tile[] tileRefArray = new Tile[requiredTiles.length];
                for (int j = 0; j < tileRefArray.length; j++) {
                    tileRefArray[j] = spritePalette.getTile(requiredTiles[j]);
                }
                sprite.render(img, tileRefArray, getCurLookup(sprite.getPaletteStart(), spriteOverlay));
            }
        }
    }

    /**
     * Returns the 4 colors associated with the supplied integer value on the
     * supplied <code>Overlay</code> object.
     * 
     * @param startValue The starting "index" of the colorset
     * @param curOverlay The <code>Overlay</code> to pull the colors from
     * @return The 4 colors that are associated with the supplied index
     */
    public Color[] getCurLookup(int startValue, Overlay curOverlay) {
        if ((startValue + 1) * 4 > 16) {
            return null;
        }
        Color[] colors = new Color[4];
        for (int i = 0; i < 4; i++) {
            Color curColor = curOverlay.getColor(startValue * 4 + i);
            colors[i] = curColor;
        }
        return colors;
    }

    /**
     * Renders the tilemap in a sequential manner with the given camera scroll
     * position.
     * <p>
     * This method is not used to render while running the game, but to properly
     * render the additional viewable nametables and other tools.
     *
     * @param cameraXLoc Camera location to render at
     * @param q          <code>Graphics</code> object to draw with
     */
    public void renderMap(int cameraXLoc, Graphics q) {
        renderTileMap(cameraXLoc, q);
    }

    private void renderTileMap(int cameraXLoc, Graphics q) {
        for (int i = (cameraXLoc / Tile.PIXELS_PER_TILE); i < Map.TILE_SCREEN_HEIGHT
                && i < Math.min(alphaTileMap[0].length, betaTileMap[0].length); i++) {
            for (int j = 0; j < Map.TILE_SCREEN_WIDTH && j < Math.min(alphaTileMap.length, betaTileMap.length); j++) {
                int[][] currTileMap = null;
                int[][] currOverlayMap = null;
                if (i < alphaTileMap.length) {
                    currTileMap = alphaTileMap;
                    currOverlayMap = alphaOverlayMap;
                } else if (i < (alphaTileMap.length + betaTileMap.length - 1)) {
                    currTileMap = betaTileMap;
                    currOverlayMap = betaTileMap;
                }
                Tile currTile = tilePalette.getTile(currTileMap[j][i]);
            }
        }
    }
}
