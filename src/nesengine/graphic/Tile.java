package nesengine.graphic;

import romparser.ByteArray;
import java.awt.Color;
import java.awt.image.BufferedImage;

public class Tile {

    /**
     * Defines the standard amount of pixels that exist on the x and y axis for
     * each tile. This value <b>must</b> be used for any tile-based painting
     * images to work with the library properly.
     */
    public static final int PIXELS_PER_TILE = 8;
    /**
     * Defines the standard amount of colors each palette should have. If a
     * palette submitted for rendering has more than this, then those extra
     * colors will not be used. If there are less, then a default "error color"
     * will be used in place of any null references.
     */
    public static final int STANDARD_NUM_COLORS = 4;
    /**
     * Defines the standard
     * <code>Tile</code> error
     * <code>Color</code>. This color will be used in the case that a palette is
     * submitted with less than the standard number of colors, and the rendered
     * image attempts to access what would normally be a null reference.
     */
    public static final Color ERR_COLOR = new Color(255, 128, 128, 0);
    /**
     * Standard color that defines the "0" marker to overwrite.
     */
    public static final Color MARKER_0 = new Color(0, 0, 0); /* new Color(0, 255, 0) */

    /**
     * Standard color that defines the "1" marker to overwrite.
     */
    public static final Color MARKER_1 = new Color(255, 0, 0); /* new Color(255, 0, 0) */

    /**
     * Standard color that defines the "2" marker to overwrite.
     */
    public static final Color MARKER_2 = new Color(0, 255, 0); /* new Color(0, 0, 255) */

    /**
     * Standard color that defines the "3" marker to overwrite.
     */
    public static final Color MARKER_3 = new Color(0, 0, 255); /* new Color(255, 255, 0) */

    /**
     * Array of standard colors that the
     * <code>Tile</code> and
     * <code>Sprite</code>
     * objects are originally composed of.
     */
    public static final Color[] DEFAULT_PALETTE = new Color[]{MARKER_0,
        MARKER_1,
        MARKER_2,
        MARKER_3
    };
    /**
     * Image type for the original generated image, without any flips or
     * translations.
     */
    public static final int TYPE_REG = 0;
    /**
     * Image type for the horizontally-flipped image.
     */
    public static final int TYPE_HORZ_FLIP = 1;
    /**
     * Image type for the vertically-flipped image.
     */
    public static final int TYPE_VERT_FLIP = 2;
    /**
     * Image type for the both vertically and horizontally flipped image.
     */
    public static final int TYPE_DUAL_FLIP = 3;
    /**
     * Byte 2D array that contains the bit data for the graphical tile. These
     * values can range from 0 through 3, which represent the 4 different
     * colors that each pixel represents.
     */
    private byte[][] img = new byte[Tile.PIXELS_PER_TILE][Tile.PIXELS_PER_TILE];

    /**
     * Standard bit-wise constructor for a
     * <code>Tile</code>. A standard
     * <code>Tile</code> is built using two supplied
     * <code>ByteArray</code> arrays of 8 objects each. The two
     * <code>ByteArray</code>s are combined, forming a generic
     * <code>Tile</code> of three different colors that act much like a
     * "heightmap," something to be drawn over.
     * <p>
     * The two
     * <code>ByteArray</code>s are combined by doing an operation similar to an
     * AND operation, but each bit is added to an integer number. Therefore, if
     * <pre>
     * byte 1 = 00101100,
     * AND
     * byte 2 = 01111000,
     *
     * then the sum will be:
     * 01212100.
     * </pre>
     * <p>
     * The second
     * <code>ByteArray</code> is weighted to 2 though, so the actual sum will be
     * <p>
     * 02323100.
     *
     * @param layer1 The first <code>ByteArray</code> array
     * @param layer2 The second <code>ByteArray</code> array
     */
    public Tile(ByteArray[] layer1, ByteArray[] layer2) {
        for (int i = 0; i < layer1.length
                && i < layer2.length && i < img.length; i++) {
            for (int j = 0; j < Tile.PIXELS_PER_TILE
                    && j < Tile.PIXELS_PER_TILE && j < img[i].length; j++) {
                img[j][i] = (byte) (layer1[i].getBit(j));
                img[j][i] += (2 * layer2[i].getBit(j));
            }
        }
    }

    /**
     * Renders the
     * <code>Tile</code> to the supplied
     * <code>BufferedImage</code>
     * object at the specified x-y location. This method is a simplified version
     * of its flip-capable counterpart.
     *
     * @param screenImg The <code>BufferedImage</code> to render to
     * @param x         The x-coordinate to draw at
     * @param y         The y-coordinate to draw at
     * @param palette   The <code>Color</code> array that contains what colors
     *                  the tile should be
     * @param isSprite  <code>true</code> if the 0th marker reference should
     *                  be treated specially (for seethrough on sprites).
     */
    public void render(BufferedImage screenImg, int x, int y, Color[] palette, boolean isSprite) {
        this.render(screenImg, x, y, palette, false, false, isSprite);
    }

    /**
     * Renders the
     * <code>Tile</code> to the supplied
     * <code>BufferedImage</code> object at the specified x-y location. The
     * drawn image can be flipped on the vertical and/or horizontal plane at
     * render-time if flipVertical or flipHorizontal are
     * <code>true</code>,
     * respectively.
     *
     * @param screenImg      The <code>BufferedImage</code> to render to
     * @param x              The x-coordinate to draw at
     * @param y              The y-coordinate to draw at
     * @param palette        The <code>Color</code> array that contains what
     *                       colors the tile should be
     * @param flipVertical   <code>true</code> if the image should be flipped
     *                       vertically
     * @param flipHorizontal <code>true</code> if the image should be flipped
     *                       horizontally
     * @param isSprite       <code>true</code> if the 0th marker reference
     *                       should be treated specially (for seethrough).
     */
    public void render(BufferedImage screenImg, int x, int y, Color[] palette, boolean flipHorizontal, boolean flipVertical, boolean isSprite) {
        if (x >= 0 && x < 256 && y >= 0 && y < 256) {
            if (!flipVertical && !flipHorizontal) {
                for (int i = 0; i < Tile.PIXELS_PER_TILE; i++) {
                    for (int j = 0; j < Tile.PIXELS_PER_TILE; j++) {
                        if (img[j][i] != 0 || !isSprite) {
                            screenImg.setRGB(x + j, y + i, palette[img[j][i]].getRGB());
                        }
                    }
                }
            } else if (flipVertical && !flipHorizontal) {
                for (int i = 0; i < Tile.PIXELS_PER_TILE; i++) {
                    for (int j = 0; j < Tile.PIXELS_PER_TILE; j++) {
                        int xIndex = (img.length - j - 1);
                        if (img[xIndex][i] != 0 || !isSprite) {
                            screenImg.setRGB(x + j, y + i, palette[img[xIndex][i]].getRGB());
                        }
                    }
                }
            } else if (!flipVertical && flipHorizontal) {
                for (int i = 0; i < Tile.PIXELS_PER_TILE; i++) {
                    for (int j = 0; j < Tile.PIXELS_PER_TILE; j++) {
                        int yIndex = (img[j].length - i - 1);
                        if (img[j][yIndex] != 0 || !isSprite) {
                            screenImg.setRGB(x + j, y + i, palette[img[j][yIndex]].getRGB());
                        }
                    }
                }
            } else {
                for (int i = 0; i < Tile.PIXELS_PER_TILE; i++) {
                    for (int j = 0; j < Tile.PIXELS_PER_TILE; j++) {
                        int xIndex = (img.length - j - 1);
                        int yIndex = (img[xIndex].length - i - 1);
                        if (img[xIndex][yIndex] != 0 || !isSprite) {
                            screenImg.setRGB(x + j, y + i, palette[img[xIndex][yIndex]].getRGB());
                        }
                    }
                }
            }
        }
    }
}
