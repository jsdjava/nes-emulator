package nesengine.graphic;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class Sprite {

    /**
     * Default x and y coordinate location denoting that this sprite should
     * be considered "invisible".
     */
    public static final int LOC_INVISIBLE = -100;
    /**
     * Integer array containing the indexes of each <code>Tile</code> within
     * the holding class. The holding class requests what <code>Tile</code>s the
     * <code>Sprite</code> needs to render, and then supplies them.
     */
    private final int[] spriteTileArray;
    /**
     * The starting "index" of the 4 colors that will be used by this sprite.
     */
    private int paletteStart;
    /**
     * Current x location of the sprite.
     */
    private int x;
    /**
     * Current y location of the sprite.
     */
    private int y;
    /**
     * Boolean that determines whether or not this sprite should be flipped
     * horizontally. This value is permanent.
     */
    private final boolean flippedHorizontal;
    /**
     * Boolean that determines whether or not this sprite should be flipped
     * vertically. This value is permanent.
     */
    private final boolean flippedVertical;

    /**
     * Method that reverses the order of all places in a supplied integer array.
     * This is useful for flipping functions.
     *
     * @param nArray The array to swap all indexes with
     * @return The swapped array
     */
    public static int[] swapArrayPlaces(int[] nArray) {
        int[] newArray = new int[nArray.length];
        for (int i = 0; i < nArray.length; i++) {
            newArray[i] = nArray[(nArray.length - i - 1)];
        }
        return newArray;
    }

    /**
     * Method that reverses the order of all places in a supplied
     * <code>Tile</code> array. This is useful for flipping functions.
     *
     * @param oArray The array to swap all indexes with
     * @return The swapped array
     */
    public static Tile[] swapArrayPlaces(Tile[] oArray) {
        Tile[] newArray = new Tile[oArray.length];
        for (int i = 0; i < oArray.length; i++) {
            newArray[i] = oArray[(oArray.length - i - 1)];
        }
        return newArray;
    }

    /**
     * Condensed version of the standard <code>Sprite</code> constructor. This
     * is left in for ease of use and for legacy code that may still use
     * previous non-flipping and default coordinate <code>Sprite</code>
     * constructors.
     * 
     * @param addr
     * @param paletteStart 
     */
    public Sprite(int addr[], int paletteStart) {
        this(0, 0, addr, paletteStart, false, false);
    }

    /**
     * Condensed version of the standard <code>Sprite</code> constructor. This
     * is left in for ease of use and for legacy code that may still use
     * previous non-flipping <code>Sprite</code> constructors.
     * 
     * @param x Starting x coordinate of the sprite
     * @param y Starting y coordinate of the sprite
     * @param addr Integer array containing index values referring to used tiles
     * @param paletteStart Index of the colorset used for this sprite
     */
    public Sprite(int x, int y, int addr[], int paletteStart) {
        this(x, y, addr, paletteStart, false, false);
    }

    /**
     * Standard constructor the <code>Sprite</code> class. Two coordinate
     * values, a sprite tile index array, an overlay starting index, and two
     * constant boolean values that determine horizontal and vertical flipping
     * are passed into the constructor.
     *
     * @param x            Starting x coordinate of the sprite
     * @param y            Starting y coordinate of the sprite
     * @param addr         Integer array containing index values referring to used tiles
     * @param paletteStart Index of the colorset used for this sprite
     * @param flippedHorz  <code>true</code> if the sprite should always be
     *                     flipped horizontally
     * @param flippedVert  <code>true</code> if the sprite should always be
     *                     flipped vertically
     */
    public Sprite(int x, int y, int addr[], int paletteStart, boolean flippedHorz, boolean flippedVert) {
        this.x = x;
        this.y = y;
        this.spriteTileArray = addr;
        this.paletteStart = paletteStart;
        this.flippedHorizontal = flippedHorz;
        this.flippedVertical = flippedVert;
    }

    /**
     * Returns an array of
     * <code>int</code>s that reference the index location of the required
     * <code>Tile</code>s stored in the
     * <code>Palette</code>.
     *
     * @return The <code>int[]</code> array of <code>Tile</code> index
     *         references
     */
    public int[] getRequiredTiles() {
        return spriteTileArray;
    }

    /**
     * Sets the x location of the
     * <code>Sprite</code> object.
     *
     * @param x New x location of the <code>Sprite</code> to render to
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Sets the y location of the
     * <code>Sprite</code> object
     *
     * @param y New y location of the <code>Sprite</code> to render to
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Sets the
     * <code>Sprite</code> to invisible. In essence, it sets the x and y
     * location to
     * <p>
     * (
     * <code>LOC_INVISIBLE</code>,
     * <code>LOC_INVISIBLE</code>),
     * <p>
     * which defines the location that is both not possible to see, and is
     * also a marker location for skipping the render function.
     */
    public void setInvisible() {
        this.x = LOC_INVISIBLE;
        this.y = LOC_INVISIBLE;
    }

    /**
     * Returns
     * <code>true</code> if the
     * <code>Sprite</code> can be defined as "invisible," otherwise
     * <code>false</code>. <i>Invisible</i> is defined as the
     * <code>Sprite</code> object has its <i>x</i> and <i>y</i> location at the
     * defined
     * <code>LOC_INVISIBLE</code> location.
     *
     * @return Whether or not the <code>Sprite</code> is invisible
     */
    public boolean isInvisible() {
        return (x == LOC_INVISIBLE && y == LOC_INVISIBLE);
    }

    /**
     * Returns the starting index location of the
     * <code>Sprite</code> object's
     * current palette. This method is used exclusively by other classes, since
     * <code>Sprite</code> does not contain its current color set.
     *
     * @return The index location of the palette
     */
    public int getPaletteStart() {
        return paletteStart;
    }

    /**
     * Renders the sprite using the supplied
     * <code>BufferedImage</code> object
     * and the supplied
     * <code>Color</code> array. If the supplied array is
     * <code>null</code>, the program renders the sprite in the
     * <code>Tile</code>
     * class' default marker colors.
     *
     * @param img                 The <code>BufferedImage</code> to draw to
     * @param tileRefArray        The array of <code>Tile</code> objects that the
     *                            <code>Sprite</code> is composed of
     * @param palette             The <code>Color</code> array that contains the
     *                            current colors of the sprite
     */
    public void render(BufferedImage img, Tile[] tileRefArray, Color[] palette) {
        if (!isInvisible()) {
            Tile[] currTileArray;
            if (flippedVertical) {
                currTileArray = swapArrayPlaces(tileRefArray);
            } else {
                currTileArray = tileRefArray;
            }
            for (int i = 0; i < currTileArray.length; i++) {
                if (palette != null) {
                    currTileArray[i].render(img, x, y + (i * Tile.PIXELS_PER_TILE), palette, flippedVertical, flippedHorizontal, true);
                } else {
                    currTileArray[i].render(img, x, y + (i * Tile.PIXELS_PER_TILE),
                            Tile.DEFAULT_PALETTE, flippedVertical, flippedHorizontal, true);
                }
            }
        }
    }
}
