package nesengine;

import javax.swing.JOptionPane;
import romparser.Processor;

public class FrontEnd extends javax.swing.JFrame {

    /**
     * Defines the standard width of the class'
     * <code>Canvas</code> object, in tiles.
     */
    public static final int CANVAS_WIDTH = 32;
    /**
     * Defines the standard height of the class'
     * <code>Canvas</code> object, in tiles.
     */
    public static final int CANVAS_HEIGHT = 30;

    public enum GameState {

        STATE_INIT,
        STATE_RUN,
        STATE_STOP,
        STATE_CONFIG,
        STATE_EXIT
    }
    private KeyMap keyMap;
    private GameState currState;
    private final Processor frontProcessor;
    private boolean startMenuActive = true;
    private boolean pauseMenuActive = false;
    private boolean resumeMenuActive = false;

    /**
     * Creates new form FrontEnd
     */
    public FrontEnd() {
        currState = GameState.STATE_INIT;
        initComponents();

        startMenuItem.setEnabled(startMenuActive);
        pauseMenuItem.setEnabled(pauseMenuActive);
        resumeMenuItem.setEnabled(resumeMenuActive);

        emulatorCanvas.createBufferStrategy(2);
        setVisible(true);
        frontProcessor = Processor.buildNESEnvironment(emulatorCanvas);
        currState = GameState.STATE_STOP;
    }

    /**
     * Returns the
     * <code>boolean</code> array of current keys pressed. Each array index
     * location refers to a certain key, and the referred key can be changed to
     * something else.
     *
     * @return The <code>boolean</code> key pressed array
     */
    public synchronized boolean[] getKeyPressed() {
        return keyMap.getKeyActive();
    }

    public void beginProgramThread() {
        Thread t;
        t = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        if (currState.equals(GameState.STATE_RUN)) {
                            long startTime = System.currentTimeMillis();
                            frontProcessor.doFrame();
                            long elapsedTime = System.currentTimeMillis() - startTime;
                            if (elapsedTime <= 50) {
                                Thread.sleep(50 - elapsedTime);
                            } else {
                                System.out.println("Shoot it took " + elapsedTime);
                            }
                        } else if (currState.equals(GameState.STATE_EXIT)) {
                            System.exit(0);
                        } else {
                            Thread.sleep(10);
                        }
                    } catch (InterruptedException e) {
                        JOptionPane.showMessageDialog(null, "For some reason, the program thread was interrupted");
                        System.exit(1);
                    }
                }
            }
        };
        t.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        emulatorCanvas = new java.awt.Canvas();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        startMenuItem = new javax.swing.JMenuItem();
        pauseMenuItem = new javax.swing.JMenuItem();
        resumeMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        keymapMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        emulatorCanvas.setPreferredSize(new java.awt.Dimension(640, 600));

        jMenu1.setText("Program");

        startMenuItem.setText("Start");
        startMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(startMenuItem);

        pauseMenuItem.setText("Pause");
        pauseMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pauseMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(pauseMenuItem);

        resumeMenuItem.setText("Resume");
        resumeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resumeMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(resumeMenuItem);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(exitMenuItem);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Tools");

        keymapMenuItem.setText("Keymap...");
        keymapMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keymapMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(keymapMenuItem);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(emulatorCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(emulatorCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void startMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startMenuItemActionPerformed
        if (startMenuActive) {
            requestStateTransition(GameState.STATE_RUN);
            startMenuActive = false;
            resumeMenuActive = false;
            pauseMenuActive = true;
            startMenuItem.setEnabled(startMenuActive);
            resumeMenuItem.setEnabled(resumeMenuActive);
            pauseMenuItem.setEnabled(pauseMenuActive);
        }
    }//GEN-LAST:event_startMenuItemActionPerformed

    private void pauseMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pauseMenuItemActionPerformed
        if (pauseMenuActive) {
            System.out.println("Pause menu deactive");
            requestStateTransition(GameState.STATE_STOP);
            resumeMenuActive = true;
            pauseMenuActive = false;
            resumeMenuItem.setEnabled(resumeMenuActive);
            pauseMenuItem.setEnabled(pauseMenuActive);
        }
    }//GEN-LAST:event_pauseMenuItemActionPerformed

    private void resumeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resumeMenuItemActionPerformed
        if (resumeMenuActive) {
            System.out.println("Resume menu deactive");
            requestStateTransition(GameState.STATE_RUN);
            resumeMenuActive = false;
            pauseMenuActive = true;
            resumeMenuItem.setEnabled(resumeMenuActive);
            pauseMenuItem.setEnabled(pauseMenuActive);
        }
    }//GEN-LAST:event_resumeMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        requestStateTransition(GameState.STATE_EXIT);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void keymapMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keymapMenuItemActionPerformed
        KeymapFront keyFront = new KeymapFront();
        keyFront.setVisible(true);
    }//GEN-LAST:event_keymapMenuItemActionPerformed

    public boolean requestStateTransition(GameState newState) {
        if (currState.equals(GameState.STATE_INIT)) {
            return false;
        } else if (currState.equals(GameState.STATE_EXIT)) {
            return false;
        }
        currState = newState;
        return true;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrontEnd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrontEnd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrontEnd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrontEnd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        FrontEnd myFront = new FrontEnd();
        myFront.beginProgramThread();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Canvas emulatorCanvas;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem keymapMenuItem;
    private javax.swing.JMenuItem pauseMenuItem;
    private javax.swing.JMenuItem resumeMenuItem;
    private javax.swing.JMenuItem startMenuItem;
    // End of variables declaration//GEN-END:variables
}
