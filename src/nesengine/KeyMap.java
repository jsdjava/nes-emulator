package nesengine;

import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KeyMap extends KeyAdapter {

    /**
     * Defines the standard name for the keymap configuration file. This file,
     * if it exists, is loaded at creation of a <code>KeyMap</code> object;
     * otherwise, this file is created and the default keys are written to the
     * file.
     */
    public static final String KEYMAP_FILE_NAME = "keymap.cfg";

    private static final Logger keymapLogger = Logger.getLogger(KeyMap.class.getName());

    private final int[] keyBind = new int[8];
    private final boolean[] keyCurrPressed = new boolean[8];
    private final File keymapFile;

    private static void remapKeysDefault(int[] keyBindings) {
        keyBindings[0] = KeyEvent.VK_A;
        keyBindings[1] = KeyEvent.VK_S;
        keyBindings[2] = KeyEvent.VK_SHIFT;
        keyBindings[3] = KeyEvent.VK_ENTER;
        keyBindings[4] = KeyEvent.VK_UP;
        keyBindings[5] = KeyEvent.VK_DOWN;
        keyBindings[6] = KeyEvent.VK_LEFT;
        keyBindings[7] = KeyEvent.VK_RIGHT;
    }

    private static int getDefaultKeycode(int index) {
        int[] tempBind = new int[8];
        remapKeysDefault(tempBind);
        return tempBind[index];
    }

    private static void parseKeymapFile(int[] keyBindings, File inputFile) {
        BufferedReader fileReader;
        if (!inputFile.exists()) {
            keymapLogger.log(Level.WARNING, "Input file was not found! Building keymap config file with defaults...");
            remapKeysDefault(keyBindings);
            writeKeymapFile(keyBindings, inputFile);
        } else {
            try {
                fileReader = new BufferedReader(new FileReader(inputFile));
                List<String> lineSet = Files.readAllLines(inputFile.toPath(), Charset.defaultCharset());
                boolean[] invalidIndicies = new boolean[keyBindings.length];
                for (int i = 0; i < lineSet.size(); i++) {
                    try {
                        String[] tokens = lineSet.get(i).split(":");
                        String prefix = tokens[0].trim();
                        String keycode = tokens[1].trim();
                        keyBindings[i] = Integer.parseInt(keycode);
                        keymapLogger.log(Level.FINE, "Parsed keycode correctly: {0}", prefix);
                        invalidIndicies[i] = false;
                    } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
                        keymapLogger.log(Level.WARNING, "Config file contained invalid line! Trying to recover...", e);
                        invalidIndicies[i] = true;
                    }
                }
                for (int j = 0; j < invalidIndicies.length; j++) {
                    if (invalidIndicies[j]) {
                        keyBindings[j] = getDefaultKeycode(j);
                    }
                }
            } catch (FileNotFoundException ex) {
                keymapLogger.log(Level.SEVERE, "An exception was encountered while trying to read from config file!", ex);
                System.out.println("Please press enter to continue...");
                try {
                    System.in.read(); //Wait for user to press enter
                } catch (IOException e) {
                    keymapLogger.log(Level.SEVERE, "Cascade failure, terminating programm immediately!");
                    System.exit(-1);
                }
                System.exit(-1);
            } catch (IOException ex) {
                keymapLogger.log(Level.SEVERE, "An exception was encountered while trying to read from the config file!", ex);
                System.out.println("Please press enter to continue...");
                try {
                    System.in.read(); //Wait for user to press enter
                } catch (IOException e) {
                    keymapLogger.log(Level.SEVERE, "Cascade failure, terminating programm immediately!");
                    System.exit(-1);
                }
            }
        }
    }

    private static void writeKeymapFile(int[] keyBindings, File outputFile) {
        PrintWriter fileWriter;
        if (!outputFile.exists()) {
            try {
                outputFile.createNewFile();
            } catch (IOException ex) {
                keymapLogger.log(Level.SEVERE, "Error encountered while creating file. Aborting program...", ex);
                System.out.println("Please press enter to continue...");
                try {
                    System.in.read();
                } catch (IOException e) {
                    keymapLogger.log(Level.SEVERE, "Cascade failure, terminating programm immediately!");
                    System.exit(-1);
                }
                System.exit(-1);
            }
        } else {
            try {
                outputFile.delete();
                outputFile.createNewFile();
            } catch (IOException ex) {
                keymapLogger.log(Level.SEVERE, "Error encountered while wiping file. Aborting program...", ex);
                System.out.println("Please press enter to continue...");
                try {
                    System.in.read();
                } catch (IOException e) {
                    keymapLogger.log(Level.SEVERE, "Cascade failure, terminating programm immediately!");
                    System.exit(-1);
                }
                System.exit(-1);
            }
        }
        try {
            fileWriter = new PrintWriter(new FileWriter(outputFile));
            for (int i = 0; i < keyBindings.length; i++) {
                fileWriter.write("KEY_" + i + ": " + keyBindings[i] + '\n');
                fileWriter.flush();
            }
        } catch (IOException ex) {
            keymapLogger.log(Level.SEVERE, "Error encountered while writing to file! Aborting program...", ex);
            System.out.println("Please press enter to continue...");
            try {
                System.in.read();
            } catch (IOException e) {
                keymapLogger.log(Level.SEVERE, "Cascade failure, terminating programm immediately!");
                System.exit(-1);
            }
            System.exit(-1);
        }

    }

    /**
     * Standard constructor for the KeyMap class.
     */
    public KeyMap() {
        keymapFile = new File(KEYMAP_FILE_NAME);
        parseKeymapFile(keyBind, keymapFile);
    }

    /**
     * Method is called by the event thread if a key on the keyboard is pressed.
     *
     * @param e The <code>KeyEvent</code> generated by the release of the key
     */
    @Override
    public void keyPressed(KeyEvent e) {
        checkKey(e);
    }

    /**
     * Method is called if a key on the keyboard is released. This method is
     * usually only called after the keyPressed method, for obvious reasons.
     *
     * @param e The <code>KeyEvent</code> generated by the release of the key
     */
    @Override
    public void keyReleased(KeyEvent e) {
        unlockKey(e);
    }

    /**
     * Changes the key binding at the specified index to the specified key.
     *
     * @param index      The index within the key bindings list to update
     * @param newKeyCode The new key to bind with
     */
    public void remapKey(int index, int newKeyCode) {
        keyCurrPressed[index] = false;
        keyBind[index] = newKeyCode;
    }

    /**
     * Returns the active key array. When a key is pressed, the program updates
     * an array of boolean values that represent different key bindings. This
     * array can be accessed to determine if a certain key that was bound to
     * an array index was pressed.
     *
     * @return The active key array
     */
    public boolean[] getKeyActive() {
        return keyCurrPressed;
    }

    private void checkKey(KeyEvent e) {
        for (int i = 0; i < keyBind.length; i++) {
            if (e.getKeyCode() == keyBind[i] && !keyCurrPressed[i]) {
                keyCurrPressed[i] = true;
            }
        }
    }

    private void unlockKey(KeyEvent e) {
        for (int i = 0; i < keyBind.length; i++) {
            if (e.getKeyCode() == keyBind[i] && keyCurrPressed[i]) {
                keyCurrPressed[i] = false;
            }
        }
    }
}
