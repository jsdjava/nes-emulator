package romparser;

public class PPUSpriteWrite extends AuxRegister {
    /**
     * This is an internal reference to the ppu, which is just used to increment
     * the ppu spot register
     */
    private PPU ppu;
    /**
     * This constructor basically creates a new ppu sprite write, it just needs
     * a reference to the current ppu object.
     * @param ppu 
     */
    public PPUSpriteWrite(PPU ppu) {
        this.ppu = ppu;
    }
    /**
     * When this register is read, it auto increments the spot inside ppu sprite spot, so that it 
     * reads from a the next value on the next read
     * @return The current ByteArray at ppu sprite spot
     */
    @Override
    public ByteArray getRegister() {
        ByteArray answer = ppu.getPPUOam().getMemory(ppu.getPPURegister(PPU.PPU_SPRITE_SPOT_REG, false).getIntValue());
        ppu.getPPUSpriteSpot().inc();
        return answer;
    }
}
