package romparser;

public class APUOam extends AuxRegister{
    
    /**
     * Gets set to true whenever getRegister is true
     */
    private boolean changed = false;
 
    /**
     * Will return the value of changed.Call to check if a loopload of sprites
     * needs to be triggered.
     * @return changed
     */
    public boolean isChanged(){
        return changed;
    }
    /**
     * This will set changed to false, generally call this once you have handled
     * the loop loading, so you don't do it twice on accident.
     */
    public void resetChanged(){
        changed = false;
    }
    /**
     * This returns the value of the address in main memory to begin copying over from.
     * @return The current value in the register.
     */
    @Override
    public ByteArray getRegister(){
        changed = true;
        return super.getRegister();
    }
}
