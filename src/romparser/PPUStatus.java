package romparser;

public class PPUStatus extends AuxRegister {
    /**
     * This represents the bit inside the status register that determines whether the ppu
     * is currently inside vblank or not.
     */
    public static final int VBLANK = 0;
    /**
     * This represents the bit inside the status register that determines whether a non transparent
     * pixel of the sprite zero is over another non transparent background pixel. Its basically
     * used for timing.
     */
    public static final int SPRITE_HIT = 1;
    /**
     * An internal reference to the ppu.
     */
    private PPU ppu;
    /**
     * This constructor pretty much just sets an internal reference to the ppu.
     * @param ppu A handle on the current ppu object.
     */
    public PPUStatus(PPU ppu) {
        this.ppu = ppu;
    }
    /**
     * This returns the value of the register, but also does some automatic stuff
     * that reads to ppu status are supposed to trigger, like ending vblank, and resetting
     * the latches inside both memory spot and scroll registers.
     * @return The value of the register(before vblank is reset)
     */
    @Override
    public ByteArray getRegister(){
        ByteArray oldRegister = new ByteArray(super.getRegister().getIntValue());
        register.setBit(VBLANK, false);//Reset vblank when it gets read
        ppu.getMemorySpot().resetLatch();
        ppu.getScroll().resetLatch();
        writtenTo = true;
        return oldRegister;
    }
    /**
     * This resets the sprite zero hit bit to false
     */
    public void resetSpriteHit(){
        register.setBit(SPRITE_HIT,false);
    }
    /**
     * This sets the sprite zero hit bit to true
     */
    public void setSpriteHit(){
        register.setBit(SPRITE_HIT, true);
    }
    /**
     * This turns off the vblank bit
     */
    public void vblankOff() {
        register.setBit(VBLANK, false);
    }
    /**
     * This triggers the vblank bit
     */
    public void vblankOn() {
        register.setBit(VBLANK, true);
    }
    /**
     * This is a simple latch that determines if the status register has been
     * written to or not. I'm mostly just using it to test different things
     * about timing involving sprite hit right now.
     */
    public boolean writtenTo = false;
    /**
     * This determines if the ppu is currently in vblank or not
     * @return True when the vblank bit is set
     */
    public boolean isVBlanking(){
        return register.getBit(VBLANK) == 1;
    }
}
