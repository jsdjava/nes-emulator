/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package romparser;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ByteLookupTable;
import java.awt.image.LookupOp;
import java.awt.image.LookupTable;
import javax.swing.JFrame;

public class RenderTest {

    public static void main(String args[]) {
        JFrame frm = new JFrame("Testing the speed that single pixels can be dynamically rendered on screen");
        Canvas cnv = new Canvas();
        frm.setSize(1500, 1000);
        cnv.setSize(frm.getSize());
        frm.add(cnv);
        frm.setVisible(true);
        cnv.createBufferStrategy(2);
        Graphics q;
        /*
        BufferedImage [][] allImgs = new BufferedImage[64][64];
        for(int i = 0; i<allImgs.length; i++){
            for(int j = 0; j<allImgs[0].length; j++){
                allImgs[i][j] = new BufferedImage(8,8,BufferedImage.TYPE_INT_ARGB);
                for(int x = 0; x<8; x++){
                    for(int y = 0; y<8; y++){
                        allImgs[i][j].setRGB(x, y, new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)).getRGB());
                    }
                }
            }
        }
        */
        BufferedImage screen = new BufferedImage(300,300,BufferedImage.TYPE_INT_ARGB);
        while (true) {
            long startTime = System.currentTimeMillis();
            q = cnv.getBufferStrategy().getDrawGraphics();
            q.setColor(Color.GREEN);
            ((Graphics2D)q).scale(4, 4);
            q.fillRect(0, 0, 300, 300);
            for(int i = 0; i<300; i++){
                for(int j = 0; j<300; j++){
                    screen.setRGB(i, j, new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)).getRGB());
                }
            }
            /*
            for(int i = 0; i<allImgs.length; i++){
                for(int j = 0; j<allImgs[0].length; j++){
                    for(int x = 0; x<8; x++){
                        for(int y = 0; y<8; y++){
                            allImgs[i][j].getRaster().setPixel(x, y, new int[]{255,(int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)});
                        }
                    }
                    q.drawImage(allImgs[i][j], i*8, j*8, null);
                }
            }
            * */
            q.drawImage(screen, 0, 0, null);
            long elapsedTime = System.currentTimeMillis() - startTime;
            q.setColor(Color.MAGENTA);
            q.setFont(new Font("Times New Roman",Font.BOLD,24));
            q.drawString("ElapsedTime "+elapsedTime, 100, 25);
            cnv.getBufferStrategy().show();
            try {
                Thread.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
