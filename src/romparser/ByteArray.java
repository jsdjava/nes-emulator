package romparser;

public class ByteArray {

    /**
     * The value being wrapped by this class, the sign bit means nothing, should
     * never be interpreted as an actual int.
     */
    private int byteValue;

    //Here to test all these bitshifts
    public static void main(String[] args) {
        ByteArray test = new ByteArray(0);
        System.out.println("Test " + test.getIntValue());
        for (int i = 0; i < 256; i++) {
            test.incValueWrap(1);
            System.out.println("test " + test.getSignedValue());
        }
    }

    /**
     * This will construct a new bytearray object, and set its value to that
     * of the int.
     *
     * @param value
     */
    public ByteArray(int value) {
        byteValue = value;
    }
    /**
     * When any of the set function(except the constructor) are called, written to is
     * set to true
     */
    private boolean writtenTo = false;

    /**
     * Determines if the value of this byte array has been changed,and then
     * automatically resets it to false.
     *
     * @return True if writtenTo is true
     */
    public boolean writtenTo() {
        boolean copyWritten = writtenTo;
        writtenTo = false;
        return copyWritten;
    }

    /**
     * Sets the current value to value
     *
     * @param value the new value for this bytearray
     */
    public void setValue(int value) {//base 10 setter
        byteValue = value;
        writtenTo = true;
    }

    /**
     * Returns whether this is 1 or 2 bytes large.
     *
     * @return 8, or 16 if it has two bits
     */
    public int getNumBits() {
        int bits = 8;
        if (byteValue / 256 != 0) {
            bits = 16;
        }
        return bits;
    }

    /**
     * Returns the value of a group of bits, from start to end, inside
     * the current value of the register.
     *
     * @param start The first index to start reading from(so like 0 or 8)
     * @param end   The index to end at+1, so 2 to 4 would return two bits
     * @return The value of those bits(combined together)
     */
    public int getBits(int start, int end) {
        int bits = getNumBits();
        int curValue = (byteValue << start) & (int) (Math.pow(2, bits) - 1);
        int bitShift = getNumBits() - (end - start);
        curValue = curValue >> bitShift;
        return curValue;
    }

    /**
     * Returns the current value of the bytearray as an int.
     *
     * @return
     */
    public int getIntValue() {
        return byteValue;
    }

    /**
     * Sets a bit inside the int to the value of the boolean
     *
     * @param index Which bit to set (0 is the lowest bit)
     * @param value True = 1, false = 0
     */
    public void setBit(int index, boolean value) {
        int bits = getNumBits();
        if (value) {
            byteValue = byteValue | ((int) Math.pow(2, bits - index - 1));
        } else {
            byteValue = byteValue & (int) ((Math.pow(2, bits) - 1) - Math.pow(2, bits - index - 1));
        }
    }
    /**
     * Similiar to set bit, this returns the value(0 or 1) of a bit inside the bytearray
     * @param index Which bit to grab (so 0 is the lowest)
     * @return The value of the bit(0 or 1)
     */
    public int getBit(int index) {
        int bits = 8;
        if (byteValue / 256 != 0) {
            bits = 16;
        }
        int curValue = byteValue >> (bits - index - 1);
        curValue = curValue & 1;
        return curValue;
    }
    /**
     * Adds a specified amount to the current value, if it becomes negative(so you
     * pass in a negative number, then it will overflow the value)
     * @param incAmt The amount to add to this (can be - or +)
     */
    public void incValue(int incAmt) {
        int bits = getNumBits();
        byteValue += incAmt;
        if (byteValue < 0) {
            byteValue = (int) Math.pow(2, bits) + byteValue;
        }
    }
    /**
     * This is similar to invValue, but if the new value needs two bytes, then this overflows
     * it back down to 1 byte.
     * @param incAmt 
     */
    public void incValueWrap(int incAmt) {
        int bits = getNumBits();
        incValue(incAmt);
        if (byteValue >= Math.pow(2, bits)) {
            byteValue = (int) (byteValue - Math.pow(2, bits));
        }
    }
    /**
     * Same as incValueWrap, except it gets the value from another ByteArray,
     * and it returns a new ByteArray with the value instead of affecting the value
     * of the current one.
     * @param bytesToAdd The ByteArray whose value you want added to the value of this byteArray
     * @return The addition of this byteArray and bytesToAdd, as a ByteArray
     */
    public ByteArray addWithWrap(ByteArray bytesToAdd) {
        int oldValue = byteValue;
        incValueWrap(bytesToAdd.getIntValue());
        ByteArray answer = new ByteArray(byteValue);
        setValue(oldValue);
        return answer;
    }
    /**
     * This is the exact same as addWithWrap, in fact I will probably just remove
     * it soon.
     * @param bytesToAdd The value you want added
     * @return A new ByteArray with the two values added.
     */
    public ByteArray addWithOverflow(ByteArray bytesToAdd) {
        return addWithWrap(bytesToAdd);
    }
    /**
     * This negates the value of bytesToSub, and then calls incValueWrap for the new
     * answer, which it wraps and returns.
     * @param bytesToSub The positive value to subtract, which will then be negated and added
     * @return The wrapped subtraction of bytesToSub and this
     */
    public ByteArray subWithOverflow(ByteArray bytesToSub) {
        int oldValue = byteValue;
        incValueWrap(-bytesToSub.getIntValue());
        ByteArray answer = new ByteArray(byteValue);
        setValue(oldValue);
        return answer;
    }
    /**
     * Does a generic and operation on two byte values.
     * @param bytesToAnd The value to and with this
     * @return The result of the and of this and bytesToAnd
     */
    public ByteArray and(ByteArray bytesToAnd) {
        int curValue = byteValue & bytesToAnd.getIntValue();
        return new ByteArray(curValue);
    }
    /**
     * Does a generic or operation on two byte values.
     * @param bytesToOr The value to or with this
     * @return The result of the or of this and bytesToOr
     */
    public ByteArray or(ByteArray bytesToOr) {
        int curValue = byteValue | bytesToOr.getIntValue();
        return new ByteArray(curValue);
    }
    /**
     * Does a generic xor operation on two byte values.
     * @param bytesToXpr The value to xor with this
     * @return The result of the xor of this and bytesToXor
     */
    public ByteArray xor(ByteArray bytesToXor) {
        int curValue = byteValue ^ bytesToXor.getIntValue();
        return new ByteArray(curValue);
    }
    /**
     * Does a generic leftShift operation on this
     * @return The result of this being shifted left 1
     */
    public ByteArray leftShift() {
        int bits = getNumBits();
        int curValue = (byteValue << 1) & (int) (Math.pow(2, bits) - 1);
        return new ByteArray(curValue);
    }
    /**
     * Does a generic leftRotate operation on this
     * @param lsb - The value to place in bit 0, since all the bits have been rotated past 0
     * @return The result of this being left rotated
     */
    public ByteArray leftRotate(int lsb) {
        int bits = getNumBits();
        int curValue = (byteValue << 1) & (int) (Math.pow(2, bits) - 1);
        curValue = curValue | lsb;
        return new ByteArray(curValue);
    }
    /**
     * Does a generic right shift operation on this
     * @return The result of this being shifted right 1
     */
    public ByteArray rightShift() {
        int bits = getNumBits();
        int curValue = (byteValue >> 1) & (int) (Math.pow(2, bits) - 1);
        return new ByteArray(curValue);
    }
    /**
     * Does a generic right rotate operation on this
     * @param msb The value to place in the final bit, since all the bits have been
     * shifted down 1.
     * @return The result of this being rotated right 1
     */
    public ByteArray rightRotate(int msb) {
        int bits = getNumBits();
        int curValue = (byteValue >> 1) & (int) (Math.pow(2, bits) - 1);
        curValue = curValue | (int) (msb * (Math.pow(2, bits - 1)));
        return new ByteArray(curValue);
    }
    /**
     * This makes the msb of this represent the sign(so 0 is positive, 1 is negative)
     * @return The signed value of this
     */
    public int getSignedValue() {
        int bits = getNumBits();
        if (getBit(0) == 0) {
            return byteValue;
        } else {
            //int curValue = byteValue << 1 & (int) (Math.pow(2, bits) - 1);
            return (int) -(Math.pow(2, bits) - byteValue);
        }
    }
    /**
     * Returns true if this has more than 8 bits
     * @return true if there are more than 8 bits
     */
    public boolean moreThan256() {
        return byteValue >= 256;
    }
}
