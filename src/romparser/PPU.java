package romparser;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import nesengine.FrontEnd;
import nesengine.graphic.Map;
import nesengine.graphic.Overlay;
import nesengine.graphic.Palette;
import nesengine.graphic.Tile;

public class PPU {

    /**
     * This is the memory address in the processor's ram that corresponds to registers
     * in the ppu.
     */
    public static final int BOT_REG_ADDR = 0x2000;
    /**
     * This is the highest memory address in the processor's ram that corresponds to registers
     * in the ppu
     */
    public static final int TOP_REG_ADDR = 0x2007;
    /**
     * This is the control register, which has the current nametable addr, the amount to increment vram
     * pattern table addrs, vbi nmi, and sprite size(which i don't even check right now).
     */
    public static final int PPU_CONTROL_REG = 0x2000;
    /**
     * This is the mask register, basically used for sprite and background colors and visibility.
     * Currently, i think the visibility stuff is the only thing implemented.
     */
    public static final int PPU_MASK_REG = 0x2001;
    /**
     * Contains stuff like VBLANK, and sprite 0 hit.(Which i think might be
     * slightly bugged) It also has sprite overflow, which is currently not implemented.
     */
    public static final int PPU_STATUS_REG = 0x2002;
    /**
     * The current address in the ppu sprite ram that will get written to next
     */
    public static final int PPU_SPRITE_SPOT_REG = 0x2003;
    /**
     * Set this to the actual value you want transferred into ppu sprite ram
     * based on the sprite spot register
     */
    public static final int PPU_SPRITE_WRITE_REG = 0x2004;
    /**
     * This determines how far to shift tiles(scrolling).
     */
    public static final int PPU_SCROLL_REG = 0x2005;
    /**
     * This is the memory address in ppu memory that will get written to next.
     */
    public static final int PPU_MEMORY_SPOT_REG = 0x2006;
    /**
     * The actual value to transfer into the memory address specified by
     * memory_spot register
     */
    public static final int PPU_MEMORY_WRITE_REG = 0x2007;
    /**
     * The number of ppu cycles in one frame
     */
    public static final int PPU_FRAMES = 89342;
    /*
     * The number of ppu cycles that this should remain in VBLANK(so basically
     * the gun on the television is being given time to travel back up to the
     * top of the screen)
     */
    public static final int VBLANK_DURATION = 6820;
    /**
     * The address in ppu ram where the pattern table for the sprites begins
     */
    public static final int PATTERN_SPRITE_ADDR = 0x0000;
    /**
     * The address in ppu ram where the pattern table for the tiles begins
     */
    public static final int PATTERN_TILE_ADDR = 0x1000;
    /**
     * The address in memory where the first name table begins
     */
    public static final int NAME_1_ADDR = 0x2000;
    /**
     * The address in memory where the second name table begins
     */
    public static final int NAME_2_ADDR = 0x2400;
    /**
     * The size of each name table
     */
    public static final int NAME_SIZE = 0x3c0;
    /**
     * The size of the attribute table at the end of each name table(its basically just
     * 64 different color regions)
     */
    public static final int NAME_COLOR_SIZE = 0x40;
    /**
     * The address in memory where the palette of colors for the tiles begins
     */
    public static final int TILE_PALETTE_START = 0x3f00;
    /**
     * This is just 1+the tile_palette_start, but it gets mirrored like every 4 addrs
     * or something, so i made it a special spot
     */
    public static final int TILE_PALETTE_0_ADDR = 0x3f01;
    /**
     * The address in memory where the palette of colors for the sprites begins
     */
    public static final int SPRITE_PALETTE_START = 0x3f10;
    /**
     * Its just 1+sprite_palette_start, same deal as with the tile_0 one
     */
    public static final int SPRITE_PALETTE_0_ADDR = 0x3f11;
    /**
     * The size of each color palette
     */
    public static final int PALETTE_SIZE = 0x10;
    /**
     * The actual object that represents the control register, handles the same stuff
     * as mention up in the PPU_CONTROL_REG
     */
    private PPUControl controlRegister = new PPUControl();
    /**
     * The actual object that represents the mask register, handles the same stuff
     * as mentioned up in the PPU_MASK_REG
     */
    private PPUMask maskRegister = new PPUMask();
    /**
     * The actual object that represents the status register, handles the same stuff
     * as the PPU_STATUS_REG
     */
    private PPUStatus statusRegister = new PPUStatus(this); //jk this shouldn't start out in vblank
    /**
     * The actual object that represents the sprite spot register, handles the same
     * stuff as PPU_SPRITE_SPOT_REG
     */
    private PPUSpriteSpot spriteSpotRegister = new PPUSpriteSpot();
    /**
     * The actual object that represents the sprite write register, handles the
     * same stuff as PPU_SPRITE_WRITE_REG
     */
    private PPUSpriteWrite spriteWriteRegister = new PPUSpriteWrite(this);
    /**
     * The actual object that represents the scroll register, handles the same
     * stuff as PPU_SCROLL_REG
     */
    private PPUScroll scrollRegister = new PPUScroll();
    /**
     * The actual object that represents the memory spot register, handles the same
     * stuff as PPU_MEM_SPOT_REG
     */
    private PPUMemorySpot memorySpotRegister = new PPUMemorySpot(this);
    /**
     * The actual object that represents the memory write register, handles the same
     * stuff as PPU_MEM_WRITE_REG
     */
    private PPUMemoryWrite memoryWriteRegister = new PPUMemoryWrite(this);
    /**
     * When get ppu register is called, changed register gets set to the register
     * that was accessed by it.Useful for updating tiles, sprites, and colors
     */
    private AuxRegister changedRegister;
    /**
     * The ppu's main ram
     */
    private ByteArray[] memory;// = new ByteArray[2][8192];
    /**
     * Wraps the ppu's special sprite ram
     */
    private PPUOam oam = new PPUOam(this);
    /**
     * This converts the palette positions into actual rgb values for each tile
     * object.
     */
    private Color[] paletteToRGB;
    /**
     * This counts every ppu processor cycle
     */
    public int ppuCycles = 0;
    /**
     * This holds the actual graphical representations for each of the tile
     * objects
     */
    private Palette tilePalette;
    /**
     * This holds the actual graphical representations for each of the sprite
     * objects
     */
    private Palette spritePalette;
    /**
     * This holds the different color schemes to be used by the tile objects
     */
    private Overlay tileOverlay;
    /**
     * This holds the different color schemes to be used by the sprite objects
     */
    private Overlay spriteOverlay;
    /**
     * This holds both nametables, and the position in the palette of each tile
     * to draw on the screen.
     */
    private Map emulatorMap;
    /**
     * This is where evertyhing is actually drawn to.
     */
    private Canvas emulatorCnv;

    /**
     * The generic constructor for a ppu nes object
     *
     * @param memory      Pass in the value to set all of the ppu ram by default.
     * @param emulatorCnv Where all the tiles and sprites will be drawn to
     */
    public PPU(ByteArray[] memory, Canvas emulatorCnv) {
        this.emulatorCnv = emulatorCnv;
        this.memory = memory;
        for(int i = 0; i<16384; i++){
          if( memory[i] == null){
            memory[i] = new ByteArray(0);
          }  
        }
        paletteToRGB = RomParser.getPaletteToColors();
        initGraphics();
    }

    /**
     * This checks to see if a memory address in the processor is actually a
     * ppu register.
     *
     * @param value The memory address to check
     * @return Whether it is a ppu register or not
     */
    public boolean isPPURegister(int value) {
        return (BOT_REG_ADDR <= value && TOP_REG_ADDR >= value);
    }

    /**
     * This gets the actual register object that is linked to the memory address
     * in the ppu
     *
     * @param value    The address of the ppu register
     * @param changing Set this to true if you want changedRegister to get set
     * @return The actual ppu register object, or null if its not a ppu register
     */
    public ByteArray getPPURegister(int value, boolean changing) {
        AuxRegister changingRegister = null;
        switch (value) {
            case PPU_CONTROL_REG:
                changingRegister = controlRegister;
                break;
            case PPU_MASK_REG:
                changingRegister = maskRegister;
                break;
            case PPU_STATUS_REG:
                if (changing) {
                    return statusRegister.getRegister();
                } else {
                    return statusRegister.displayRegister();
                }
            case PPU_SPRITE_SPOT_REG:
                changingRegister = spriteSpotRegister;
                break;
            case PPU_SPRITE_WRITE_REG:
                changingRegister = spriteWriteRegister;
                break;
            case PPU_SCROLL_REG:
                changingRegister = scrollRegister;
                break;
            case PPU_MEMORY_SPOT_REG:
                changingRegister = memorySpotRegister;
                break;
            case PPU_MEMORY_WRITE_REG:
                changingRegister = memoryWriteRegister;
                break;
        }
        if (changing) {
            changedRegister = changingRegister;
        }
        ByteArray res = changingRegister.getRegister();
        if(res == null){
            System.out.println("???????");
        }
        return res;
    }

    /**
     * Returns the ppu memory spot register object
     *
     * @return ppu memory spot register
     */
    public PPUMemorySpot getMemorySpot() {
        return memorySpotRegister;
    }

    /**
     * Returns the ppu control register object
     *
     * @return ppu control spot register
     */
    public PPUControl getPPUControl() {
        return controlRegister;
    }

    /**
     * Returns the ppu sprite spot register object
     *
     * @return ppu sprite spot register
     */
    public PPUSpriteSpot getPPUSpriteSpot() {
        return spriteSpotRegister;
    }
    /**
     * Used to render each pixel from tiles and sprites onto the screen.
     */
    private Graphics q;
    /**
     * Used to prevent sprite zero from being set more than once a frame
     */
    private boolean spriteZeroLatch = false;
    /**
     * Counts the number of ppu frames
     */
    private int cycles = 0;
    /**
     * Used to set each specific pixel from tiles and sprites, is drawn onto the
     * q object, which puts it all on the screen canvas.
     */
    private BufferedImage screenImg = new BufferedImage(300, 300, BufferedImage.TYPE_INT_ARGB);

    /**
     * Should get called 3x every processor cycle, basically just updates memory graphics,
     * and renders a single pixel or just waits based on ppuCycles
     */
    public void update() {
        oam.update();
        scrollRegister.update();
        if (changedRegister != null && changedRegister.equals(memorySpotRegister)) {
            //System.out.println("Writing to mem spot at " + ppuCycles);
        }
        changedRegister = null;
        if (q == null) {
            q = emulatorCnv.getBufferStrategy().getDrawGraphics();
            ((Graphics2D) q).scale(((double) emulatorCnv.getWidth() / FrontEnd.CANVAS_WIDTH) / Tile.PIXELS_PER_TILE,
                    ((double) emulatorCnv.getHeight() / FrontEnd.CANVAS_HEIGHT) / Tile.PIXELS_PER_TILE);
            q.setColor(Color.BLUE);
            q.fillRect(0, 0, 500, 500);
            //System.out.println("Cycle has begun");
        }
        if (ppuCycles < PPU_FRAMES) {
            int maxPixel = 341 * 241; //I'm slaughtering this right now, should come back and rewrite appropriately
            if (ppuCycles == 0) {
                statusRegister.resetSpriteHit();
                statusRegister.vblankOff();
            }
            if (ppuCycles < PPU_FRAMES - VBLANK_DURATION) { //This might need to be >=, highly doubt it matters, just don't want the CPU to miss a vblank basically
                //ppuCycles -= VBLANK_DURATION;
                if (ppuCycles < maxPixel) {
                    int curX = ppuCycles % (341);
                    int curY = ppuCycles / (341);
                    if (curX < 256 && curY < 240) {
                        statusRegister.writtenTo = false;
                        int inc = 0;
                        if (curX % Tile.PIXELS_PER_TILE == 0 && curY % Tile.PIXELS_PER_TILE == 0) {
                            if (curX == 24 && curY == 16) {
                                if (scrollRegister.getScrollX() != 0 || controlRegister.getNameTableSpot() > 0) {
                                    //    System.out.println("scrollRegister x "+controlRegister.getNameTableSpot());
                                    //    System.out.println("scrollRegister "+controlRegister.getOtherNameTableSpot());
                                }
                            }
                            //emulatorMap.renderMap(q); //Should be render tile
                            // System.out.println("At "+curX+" x scrollX is "+scrollRegister.getScrollX());
                            if (controlRegister.getNameTableSpot() > 0) {
                                inc = 255;
                            }
                            if (maskRegister.isRendering()) {
                                emulatorMap.renderNextTile(screenImg, scrollRegister.getScrollX() + inc, scrollRegister.getScrollY());
                            } else {
                                emulatorMap.incNextTile();
                            }
                        }
                        if (getSpritePixel(curX, curY, PPUOam.SPRITE_ZERO_SPOT) && getTilePixel(curX + inc + scrollRegister.getScrollX(), curY) && !spriteZeroLatch) {
                            spriteZeroLatch = true;
                            q.setColor(Color.MAGENTA);
                            //for (int i = curX - 15; i < curX; i++) {
                            //if (i > 0) {
                            //    screenImg.setRGB(i, curY, new Color(255, 255, 255).getRGB());
                            //}
                            //}
                            statusRegister.setSpriteHit();
                        }
                    } else if (controlRegister.displayRegister().getBit(0) == 0) {
                        //I think we're hblanking in here...
                        //System.out.println("curX "+curX);
                        //System.out.println("curY "+curY);
                        if (statusRegister.writtenTo) {
                            //System.out.println("curX "+curX+" curY "+curY);
                            statusRegister.writtenTo = false;
                            //controlRegister.getRegister().setBit(PPUControl.BASE_TABLE_ADDR_1, false);
                        }
                    }
                }
                //ppuCycles += VBLANK_DURATION;
            }
            ppuCycles++;
            if (ppuCycles == maxPixel) {
                cycles++;
                spriteZeroLatch = false;
                emulatorMap.renderSpriteList(screenImg);
                q.drawImage(screenImg, 0, 0, null);
                //Rectangle spriteZero = oam.getSpriteSize(0);
                //q.setColor(Color.WHITE);
                //q.fillRect(spriteZero.x, spriteZero.y, spriteZero.width, spriteZero.height);
                emulatorMap.resetTileScan();
                if (maskRegister.isRendering()) {
                    emulatorCnv.getBufferStrategy().show();
                }
                q = null;
            }
            if (maskRegister.isRendering() && ppuCycles == PPU_FRAMES - 2 && cycles % 2 == 0) {
                ppuCycles++;
            }
            if (ppuCycles == 82183) {
                statusRegister.vblankOn();
            }
        } else {
            ppuCycles = 0;
        }
        int changedSpot;
        if ((changedSpot = memoryWriteRegister.getChangedSpot()) > 0) { //Something got written to, need to figure out where it is
            if (memory[changedSpot].writtenTo()) {
                memorySpotRegister.incWriteSpot();
            } else {
                memorySpotRegister.incReadSpot();
            }
            if (changedSpot == 8316) {
                //System.out.println("Changed Spot is "+changedSpot);
                //System.out.println("Changed Spot value is "+memory[changedSpot].getIntValue());
            }
            if (changedSpot < PATTERN_TILE_ADDR) { //Its sprite palette stuff
                spritePalette.setTile(changedSpot / 16, getPaletteTile(changedSpot));
            } else if (changedSpot < NAME_1_ADDR) {  //Its tile palette stuff
                tilePalette.setTile((changedSpot - PATTERN_TILE_ADDR) / 16, getPaletteTile((changedSpot)));
            } else if (changedSpot < NAME_2_ADDR && changedSpot - NAME_1_ADDR <= NAME_SIZE) {  //Its Name table 1 tile stuff(emulatorMap)
                emulatorMap.setSpot(changedSpot - NAME_1_ADDR, memory[changedSpot].getIntValue());
            } else if (changedSpot < NAME_2_ADDR) { //Name table 1 color stuff
                setAttributeSpot(changedSpot, 0);
            } else if (changedSpot < NAME_2_ADDR + NAME_SIZE) { //Its name table 2 tile stuff(emulatorMap)
                emulatorMap.setSpot(changedSpot - NAME_1_ADDR - NAME_COLOR_SIZE, memory[changedSpot].getIntValue());
            } else if (changedSpot < NAME_2_ADDR + NAME_SIZE + NAME_COLOR_SIZE) { //Its name table 2 color stuff
                setAttributeSpot(changedSpot, 1);
            } else if (changedSpot >= TILE_PALETTE_START && changedSpot < TILE_PALETTE_START + PALETTE_SIZE) {
                if (changedSpot % 4 == 0) {//Its a back color mirror
                    if (changedSpot - TILE_PALETTE_START == 0) {
                        for (int i = 0; i < PALETTE_SIZE; i += 4) {
                            tileOverlay.setColor(i, paletteToRGB[memory[changedSpot].getIntValue()]);
                            spriteOverlay.setColor(i, paletteToRGB[memory[changedSpot].getIntValue()]);
                        }
                    }
                } else {
                    tileOverlay.setColor(changedSpot - TILE_PALETTE_START, paletteToRGB[memory[changedSpot].getIntValue()]);
                }
            } else if (changedSpot >= SPRITE_PALETTE_START && changedSpot < SPRITE_PALETTE_START + PALETTE_SIZE) {
                if (changedSpot % 4 == 0) {//Its a back color mirror
                    if (changedSpot - SPRITE_PALETTE_START == 0) {
                        for (int i = 0; i < PALETTE_SIZE; i += 4) {
                            tileOverlay.setColor(i, paletteToRGB[memory[changedSpot].getIntValue()]);
                            spriteOverlay.setColor(i, paletteToRGB[memory[changedSpot].getIntValue()]);
                        }
                    }
                } else {
                    spriteOverlay.setColor(changedSpot - SPRITE_PALETTE_START, paletteToRGB[memory[changedSpot].getIntValue()]);
                }
            } else {
                //System.out.println("Writing out to an unused portion of the ppu " + new BigInteger("" + changedSpot, 10).toString(16));
            }
        }
        //emulatorMap.setScrollVals(scrollRegister.getScrollX(), scrollRegister.getScrollY(), controlRegister.getNameTableSpot());
    }

    /**
     * Determines if an nmi needs to be triggered inside the processor
     *
     * @return True if there is an nmi(non-maskable interrupt)
     */
    public boolean callNMI() {
        if (controlRegister.getRegister().getBit(PPUControl.NMI_AT_VBI) == 1) {
            if (statusRegister.isVBlanking()) {
                //controlRegister.getRegister().setBit(PPUControl.NMI_AT_VBI, false); //Not sure about this, just trying to stop the interrupt from being called infinitely
                statusRegister.vblankOff();
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the ppu oam, which is basically sprite ram
     *
     * @return
     */
    public PPUOam getPPUOam() {
        return oam;
    }

    /**
     * Returns the ppu mask register
     *
     * @return The ppu mask register object
     */
    public PPUMask getPPUMask() {
        return maskRegister;
    }

    /**
     * Sets an address in the ppu's main ram
     *
     * @param addr  The address to set
     * @param value The value to set it to
     */
    public void setMemory(int addr, int value) {
        memory[addr].setValue(value);
    }

    /**
     * Returns the value of a memory address in the ppu's ram
     *
     * @param addr The address to access
     * @return The value at that memory address
     */
    public ByteArray getMemory(int addr) {
        return memory[addr];
    }

    /**
     * Returns the map object that holds the actual tile objects that are
     * rendered on screen
     *
     * @return The map object
     */
    public Map getMap() {
        return emulatorMap;
    }

    /**
     * Loads palettes, overlays, and map objects with default values
     */
    private void initGraphics() {
        tileOverlay = loadOverlay(TILE_PALETTE_START);
        spriteOverlay = loadOverlay(SPRITE_PALETTE_START);
        tilePalette = loadPalette(PATTERN_TILE_ADDR);
        spritePalette = loadPalette(PATTERN_SPRITE_ADDR);
        emulatorMap = initMap();
    }

    /**
     * Loads each overlay object with default values in memory.
     *
     * @param startAddr The address to start loading from
     * @return A overlay object loaded with the values starting at startAddr
     */
    private Overlay loadOverlay(int startAddr) {
        Color[] colors = new Color[PALETTE_SIZE];
        for (int i = startAddr; i < startAddr + PALETTE_SIZE; i++) {
            colors[i - startAddr] = Color.BLACK;
            if(memory[i]!=null){
                int colorSpot = memory[i].getIntValue();
                if (colorSpot < paletteToRGB.length) {
                    colors[i - startAddr] = paletteToRGB[colorSpot];
                }
            }
        }
        return new Overlay(colors);
    }

    /**
     * Loads each palette object with default values in memory.
     *
     * @param startAddr The address to start loading from
     * @return A palette object loaded with values starting at startAddr
     */
    private Palette loadPalette(int startAddr) {
        Palette palette = new Palette();
        for (int j = 0; j < PATTERN_TILE_ADDR / (Tile.PIXELS_PER_TILE * 2); j++) {
            int curTileShift = j * Tile.PIXELS_PER_TILE * 2;
            palette.addTile(getPaletteTile(curTileShift + startAddr));
        }
        return palette;
    }

    /**
     * When an overlay's colors change, this gets called
     *
     * @param changedSpot The overlay position that was changed
     * @param table       Which nametable it was changed on(so 0 or 1)
     */
    private void setAttributeSpot(int changedSpot, int table) {
        int beginSpot = 0;
        switch (table) {
            case 0:
                beginSpot = (changedSpot - NAME_1_ADDR - NAME_SIZE);
                break;
            case 1:
                beginSpot = (changedSpot - NAME_2_ADDR - NAME_SIZE);
                break;
        }
        int y = beginSpot / Tile.PIXELS_PER_TILE;
        int x = (beginSpot - y * Tile.PIXELS_PER_TILE);
        int counter = 0;
        for (int j = y * 2 + 1; j >= y * 2; j--) {
            for (int i = x * 2 + 1; i >= x * 2; i--) {
                int curValue = memory[changedSpot].getBits(counter * 2, (counter + 1) * 2);
                for (int a = 0; a <= 1; a++) {
                    for (int b = 0; b <= 1; b++) {
                        int curSpot = (j * 2 + a) * SCREEN_TILES_X + i * 2 + b;
                        if (curSpot < NAME_SIZE) {
                            emulatorMap.setOverlaySpot(curSpot + table * NAME_SIZE, curValue);
                        }
                    }
                }
                counter++;
            }
        }
    }

    /**
     * Creates a tile object from an address in memory using two channels
     *
     * @param addr The address to start loading it from
     * @return A new tile object wrapping the values found starting at addr.
     */
    private Tile getPaletteTile(int addr) {
        ByteArray[] rows1 = new ByteArray[Tile.PIXELS_PER_TILE];
        ByteArray[] rows2 = new ByteArray[Tile.PIXELS_PER_TILE];
        for (int i = 0; i < Tile.PIXELS_PER_TILE; i++) {
            rows1[i] = new ByteArray(0);
            rows2[i] = new ByteArray(0);
            if(memory[addr+i]!=null){
              rows1[i] = memory[addr + i];
            }
            if(memory[addr + i + Tile.PIXELS_PER_TILE] !=null ){
              rows2[i] = memory[addr + i + Tile.PIXELS_PER_TILE];
            }
        }
        return new Tile(rows1, rows2);
    }
    /**
     * The number of tiles that fit in a screen in the x direction
     */
    private int SCREEN_TILES_X = 32;
    /**
     * The number of tiles that fit in a screen in the y direction
     */
    private int SCREEN_TILES_Y = 30;

    /**
     * Initializes a map object with the palette and overlay objects, also loads up
     * the correct tiles based on the 1st nametable.
     *
     * @return The map object with tiles, overlays, and palettes in it.
     */
    private Map initMap() {    //It looks like actual maps are loaded left to right, top to bottom, 30x32 bytes = (960)
        int[][] curMapTiles = new int[SCREEN_TILES_X][SCREEN_TILES_Y];
        int[][] nextMapTiles = new int[SCREEN_TILES_X][SCREEN_TILES_Y]; //Not implemented right now
        int memSpot = NAME_1_ADDR;
        for (int j = 0; j < curMapTiles[0].length; j++) {
            for (int i = 0; i < curMapTiles.length; i++) {
                curMapTiles[i][j] = 0;
                nextMapTiles[i][j] = 0;
                if(memory[memSpot] !=null){
                  curMapTiles[i][j] = memory[memSpot].getIntValue();
                  nextMapTiles[i][j] = memory[memSpot].getIntValue();
                }
                memSpot++;
            }
        }
        Map map = new Map(tilePalette, spritePalette, tileOverlay, spriteOverlay);
        int[][] curMapOverlay = new int[SCREEN_TILES_X][SCREEN_TILES_Y];
        int[][] nextMapOverlay = new int[SCREEN_TILES_X][SCREEN_TILES_Y];
        map.writeAlphaScreen(curMapTiles, curMapOverlay);
        map.writeBetaScreen(nextMapTiles, nextMapOverlay);
        return map;
    }

    /**
     * This is kind of broken right now, but it returns a name table filled with
     * each of the tiles in the palette, very useful for debugging.
     *
     * @return A nametable that just shows each tile in the palette in sequential order
     */
    public Map getTileNameTableMap() {
        int[][] tiles = new int[16][16];
        for (int j = 0; j < tiles.length; j++) {
            for (int i = 0; i < tiles[j].length; i++) {
                tiles[i][j] = j * tiles[0].length + i;
            }
        }
        Map tileTableMap = new Map(tilePalette, tilePalette, tileOverlay, spriteOverlay);
        tileTableMap.writeAlphaScreen(tiles, tiles);
        tileTableMap.writeBetaScreen(tiles, tiles);
        return tileTableMap;
    }

    /**
     * This is also sort of borken, but it returns a map with each of the sprites in
     * the palette, in sequential order.
     *
     * @return A map of all sprites in the sprite palette.
     */
    public Map getSpriteNameTableMap() {
        int[][] tiles = new int[16][16];
        for (int j = 0; j < tiles.length; j++) {
            for (int i = 0; i < tiles[j].length; i++) {
                tiles[i][j] = j * tiles[0].length + i;
            }
        }
        Map spriteTableMap = new Map(spritePalette, spritePalette, tileOverlay, spriteOverlay);
        spriteTableMap.writeAlphaScreen(tiles, tiles);
        spriteTableMap.writeBetaScreen(tiles, tiles);
        return spriteTableMap;
    }

    /**
     * Returns the ppu scroll register
     *
     * @return The ppu scroll register object
     */
    public PPUScroll getScroll() {
        return scrollRegister;
    }

    /**
     * Returns the pixel value inside a tile
     *
     * @param x The x coordinate of the pixel(in pixels)
     * @param y The y coordinate of the pixel(in pixels)
     * @return The value of the tile(so if its 0 or not)
     */
    private boolean getTilePixel(int x, int y) {
        //Translate x and y to tile coords
        int curTable = NAME_1_ADDR;
        if (x >= 512) {
            x -= 512;
        } else if (x >= 256) {
            x -= 256;
            curTable = NAME_2_ADDR;
        }
        int tileX = x / Tile.PIXELS_PER_TILE;
        int tileY = y / Tile.PIXELS_PER_TILE;
        int memSpot = curTable + tileY * SCREEN_TILES_X + tileX;
        int paletteSpot = memory[memSpot].getIntValue();
        return getPalettePixel(paletteSpot + PATTERN_TILE_ADDR / 16, x - (tileX * Tile.PIXELS_PER_TILE), y - (tileY * Tile.PIXELS_PER_TILE));
    }

    /**
     * Generally used with getSprite and getTile pixel, checks a pixel inside
     * the palette.
     *
     * @param memSpot The spot in palette memory to look
     * @param x       The x inside the palette spot, i think this should be less than 16
     * @param y       The y inside the palette spot, should also be less than 16
     * @return If the pixel is 0 or not
     */
    private boolean getPalettePixel(int memSpot, int x, int y) {
        int paletteSpot = memSpot; //Sometimes will also be NAME_2
        if( memory[(paletteSpot * 16 + y)] ==null || memory[(paletteSpot * 16 + y + 8)] == null){
          return false;
        }
        int lowValue = memory[(paletteSpot * 16 + y)].getBit(x);
        int highValue = memory[(paletteSpot * 16 + y + 8)].getBit(x);
        //System.out.println("y "+y+" x "+x);
        //System.out.println("lowvalue "+lowValue+" highValue "+highValue);
        return lowValue + highValue * 2 > 0;
    }

    /**
     * Gets the pixel value inside a sprite
     *
     * @param x          The x inside the sprite
     * @param y          The y inside the sprite
     * @param spriteSpot The spot in sprite ram(0-64) to check
     * @return If the pixel is 0 or not
     */
    private boolean getSpritePixel(int x, int y, int spriteSpot) {
        Rectangle spriteBounds = oam.getSpriteSize(spriteSpot);
        if (spriteBounds.contains(new Point(x, y))) {
            int shiftX = x - spriteBounds.x;
            int shiftY = y - spriteBounds.y;
            return getPalettePixel(oam.getSpritePattern(0, spriteSpot), shiftX, shiftY);
        }
        return false;
    }
}
