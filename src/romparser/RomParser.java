/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package romparser;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

public class RomParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        parseROM();
    }
    private static HashMap opToAsm;
    private static HashMap opToSize;
    private static HashMap opToMode;

    public static HashMap getOpToAsm() {
        return opToAsm;
    }

    public static HashMap getOpToSize() {
        return opToSize;
    }

    public static HashMap getOpToMode() {
        return opToMode;
    }

    public static ArrayList<ByteArray[]> parseROM() {
        ArrayList<ByteArray[]> instructionsList = new ArrayList<ByteArray[]>();
        try { 
            InputStream romIn = new FileInputStream(new File("<ROM PATH GOES HERE>"));
            BufferedReader reader = new BufferedReader(new FileReader("./opcodes.csv"));
            String curLine;
            opToAsm = new HashMap();
            opToSize = new HashMap();
            opToMode = new HashMap();
            while ((curLine = reader.readLine()) != null) {
                if (!curLine.replaceAll(",", "").equals("")) {
                    String[] vals = curLine.split(",");
                    if (!vals[0].startsWith("#")) {
                        opToAsm.put(new BigInteger(vals[0], 16).intValue(), vals[1]);
                        opToMode.put(new BigInteger(vals[0], 16).intValue(), vals[2]);
                        opToSize.put(new BigInteger(vals[0], 16).intValue(), Integer.parseInt(vals[3]));
                    }
                }
            }
            int curByte;
            //Skip the first 4 bytes, they're just NES0x123
            romIn.skip(4);
            int prgBlocks = romIn.read();
            int chrBlocks = romIn.read();
            System.out.println("# of program blocks " + prgBlocks);
            System.out.println("# of character(data) blocks " + chrBlocks);
            romIn.skip(10);
            int offset = 16;
            while ((curByte = romIn.read()) != -1) {
                if ((offset - 16) < 16384 * prgBlocks) {
                    if (opToAsm.containsKey(curByte) && offset <= 65342) {
                        //System.out.println(opToAsm.get(curByte) + " skips " + opToSize.get(curByte));
                        ByteArray[] curInstr = new ByteArray[(int) opToSize.get(curByte) + 1];
                        curInstr[0] = new ByteArray(0);
                        curInstr[0].setValue(curByte);
                        for (int i = 0; i < curInstr.length - 1; i++) {
                            curInstr[i + 1] = new ByteArray(0);
                            curInstr[i + 1].setValue(romIn.read());
                        }
                        instructionsList.add(curInstr);
                        //romIn.skip((int) opToSize.get(curByte));
                        offset += (int) opToSize.get(curByte);
                    } else {
                        ByteArray[] nonInstr = new ByteArray[1];
                        nonInstr[0] = new ByteArray(curByte);
                        instructionsList.add(nonInstr);
                        //System.out.println(curByte + " is at offset " + (offset));
                    }
                } else if ((offset - 16) < 16384 * prgBlocks + 8192 * chrBlocks) {
                    int chrOffset = (offset - 16) - (16384 * prgBlocks);
                    //System.out.println("curbyte "+curByte);
                    //System.out.println("Chr Offset "+chrOffset);
                    //System.out.println("File Offset "+offset);
                    sprites[chrOffset] = new ByteArray(curByte);

                } else {
                    System.out.println("Reached end (last 128 or less thing)");
                }
                offset++;
            }
            System.out.println("End of file was " + offset + " bytes");
            romIn.close();
            reader.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error occurred while reading either the nes file, or the opcodes.csv file.");
            System.exit(1);
        }
        return instructionsList;
    }
    private static ByteArray[] sprites = new ByteArray[16384];
    //Plz only call this after using parse rom

    public static ByteArray[] getSprites() {
        return sprites;
    }
    private static Color[] paletteToRGB = new Color[64];

    public static Color[] getPaletteToColors() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("./colors.csv"));
            for (int i = 0; i < paletteToRGB.length; i++) {
                String[] colorStrs = reader.readLine().split(",");
                paletteToRGB[i] = new Color(Integer.parseInt(colorStrs[0]), Integer.parseInt(colorStrs[1]), Integer.parseInt(colorStrs[2]));
            }
            reader.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Can't read the colors.csv file for some reason");
            System.exit(1);
        }
        return paletteToRGB;
    }

    public static String printChannel(Object[] channel) {
        String answer = "";
        for (Object i : channel) {
            answer += i + " ";
        }
        return answer;
    }

    public static String getBits(int number) {
        String answer = "";
        while (number > 0 && answer.length() < 8) {//should just overflow
            answer = number % 2 + answer;
            number = number / 2;
        }
        while (answer.length() < 8) {
            answer = "0" + answer;
        }
        return answer;
    }
}
