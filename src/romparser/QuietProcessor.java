/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package romparser;

import java.awt.Canvas;
import java.util.ArrayList;

public class QuietProcessor extends Processor {

    public QuietProcessor(ByteArray[] instructions, ByteArray[] sprites, Canvas renderCnv,boolean mirrorPrgBlocks) {
        super(instructions, sprites, renderCnv,mirrorPrgBlocks);
    }

    public static void main(String args[]) {
        ArrayList<ByteArray[]> instructionsList = RomParser.parseROM();
        ByteArray[] sprites = RomParser.getSprites();
        ByteArray[] instructions = new ByteArray[81920];
        int counter = 0;
        EXIT_INSTR_COPY:
        for (ByteArray[] curRow : instructionsList) {
            for (ByteArray curByte : curRow) {
                instructions[counter] = curByte;
                counter++;
                if (counter >= instructions.length) {
                    break EXIT_INSTR_COPY;
                }
            }
        }
        counter--;
        boolean mirrorPrgBlocks = counter<=16384;
        for (ByteArray b : sprites) {
            instructions[counter] = b;
            counter++;
        }
        NESEmulatorFrontEnd nefe = new NESEmulatorFrontEnd();
        QuietProcessor curProcessor = new QuietProcessor(instructions, sprites, nefe.getCanvas(),mirrorPrgBlocks);
        nefe.start(curProcessor.loadCurrentMap());
        while (true) {
            try {
                long startTime = System.currentTimeMillis();
                curProcessor.doFrame();
                long elapsedTime = System.currentTimeMillis() - startTime;
                if (elapsedTime <= 50) {
                    Thread.sleep(50 - elapsedTime);
                } else {
                    System.out.println("Shoot it took " + elapsedTime);
                }
                //System.out.println("it took  "+(System.currentTimeMillis() - curTime)+" ms to finish "+PPU.PPU_FRAMES+" processor cycles");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
