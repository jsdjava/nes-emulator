package romparser;

import java.awt.Rectangle;
import nesengine.graphic.Sprite;
import nesengine.graphic.Tile;

public class PPUOam {

    /**
     * This is multiplied by the offset value in ppu sprite spot in order
     * to determine where to begin the dma sequence.
     */
    public static final int OFFSET_MULT = 0x100;
    /**
     * This is sort of stupid, but it cleans up magic constants. Its the maximum
     * different bit combinations of a byte(2^8)
     */
    public static final int BYTE_SIZE = 0x100;
    /**
     * This is the amount of sprite ram in bytes, which divided by 4(which is the
     * number of bytes per sprite) yields 64, which is the maximum number of sprites
     */
    public static final int OAM_SIZE = 0x100;
    /**
     * This is the position in ram of the first sprite, which not suprisingly enough is zero
     */
    public static final int SPRITE_ZERO_SPOT = 0;//duh 
    /**
     * This is the offset from a base sprite position to read the sprite's x value.
     */
    public static final int SPRITE_X = 3;
    /**
     * This is the offset from a base sprite position to read the sprite's y value
     */
    public static final int SPRITE_Y = 0;
    /**
     * This is the offset from a base sprite position to read the sprite's address
     * in the sprite palette
     */
    public static final int SPRITE_ADDR = 1;
    /**
     * This is the bit inside a sprite addr byte that represents the lower bit of the
     * color palette to read its colors from.
     */
    public static final int SPRITE_PALETTE_0 = 7;
    /**
     * This is the bit insde a sprite addr byte that represents the upper bit of the color palette
     * to read its colors from.
     */
    public static final int SPRITE_PALETTE_1 = 6;
    /**
     * This is the bit inside a sprite attr byte that represents whether
     * the sprite should be flipped horizontally.
     */
    public static final int SPRITE_FLIP_HORI = 1;
    /**
     * This is the bit inside a sprite attr byte that represents whether the sprite
     * should be flipped vertically.
     */
    public static final int SPRITE_FLIP_VERT = 0;
    /**
     * This is the offset from a base sprite position that specifies the byte at which
     * different attributes of the sprite, like orientation, are stored.
     */
    public static final int SPRITE_ATTR = 2;
    /**
     * This is another bit inside a sprite attr byte, that specifies the size of the sprite
     * (either 8 or 16 px tall). It is currently unused
     */
    public static final int SPRITE_SIZE = 4;
    /**
     * This represents the pixel size of a sprite in the x axis
     */
    public static final int SPRITE_PX_X = 8;
    /**
     * This represents the pixel size of a sprite in the y axis
     */
    public static final int SPRITE_PX_Y = 8;
    /**
     * This wraps all the different sprite bytes that are used to update the actual
     * graphical sprite objects.
     */
    private ByteArray[] oam = new ByteArray[OAM_SIZE];
    /**
     * This is just an internal reference to the ppu.
     */
    private PPU ppu;

    /**
     * The constructor to make a ppuoam object. It needs the ppu so that it can
     * set different graphical values stored in the ppu.
     *
     * @param ppu A handle on the current ppu.
     */
    public PPUOam(PPU ppu) {
        for (int i = 0; i < oam.length; i++) {
            oam[i] = new ByteArray(0);
        }
        this.ppu = ppu;
    }
    /**
     * This int is used by the dma when it calls loop load. It basically just stores
     * the current position in sprite ram, and increments until it reaches the end
     * of sprite ram and resetLoad is called.
     */
    private int spot = 0;

    /**
     * Call this every loop inside a dma cycle. It loads bytes sequentially into each
     * spot inside the ppuoam byte array.
     *
     * @param value The value to be loaded in the current spot value of oam.
     */
    public void loopLoad(ByteArray value) {
        oam[spot] = value;
        if ((spot) % 4 == 0) { //We need to fix its spot in the actual map
            updateSprite(spot);
        }
        spot++;
    }

    /**
     * This is called every time a sprite inside ppu oam is changed. It updates
     * its graphical counterpart inside the ppu.
     *
     * @param spot The spot(from 0 - 64) to change inside the ppu map of sprites
     */
    private void updateSprite(int spot) {
        //System.out.println("Loading at spot "+spot);
        Rectangle spriteSize = getSpriteSize(spot);
        int[] spriteAddrs = {getSpritePattern(0, spot)};
        if (spriteSize.height > SPRITE_PX_Y) {
            spriteAddrs = new int[2];
            spriteAddrs[0] = getSpritePattern(0, spot / SPRITE_SIZE);
            spriteAddrs[1] = getSpritePattern(1, spot / SPRITE_SIZE);
        }
        //System.out.println("Sprite hitbox "+spriteSize);
        //System.out.println("Sprite name table addr "+spriteAddrs[0]);
        ppu.getMap().setSprite(new Sprite(spriteSize.x, spriteSize.y, spriteAddrs, getSpritePalette(spot), getSpriteFlippedHori(spot), getSpriteFlippedVert(spot)), spot / SPRITE_SIZE);
    }

    /**
     * Call this at the end of a dma cycle in order to return the current spot in ppu oam
     * to 0
     */
    public void resetLoad() {
        spot = 0;
    }
    /**
     * This is the bit inside a sprite addr byte that specifies whether the palette to be used
     * for this sprite is the sprite or tile palette.
     */
    public static final int SPRITE_ADDR_BANK_BIT = 7;

    /**
     * This determines if a sprite was written to at some point during the previous ppu cycle,
     * and updates the appropriate Sprite object inside the ppu's map of sprites.Should be called
     * at the end of every ppu cycle.
     */
    public void update() {
        if (changedSpot > 0) {
            updateSprite(changedSpot / SPRITE_SIZE * SPRITE_SIZE);
        }
        changedSpot = -1;
    }
    /**
     * A simple way of marking what spot, if any, has been changed(should be -1 if nothing happened)
     */
    private int changedSpot = -1;

    /**
     * Returns the ByteArray at the spot in memory(so 0 -255)
     *
     * @param spot The position of the byte
     * @return The bytearray at that position
     */
    public ByteArray getMemory(int spot) {
        changedSpot = spot;
        return oam[spot];
    }

    /**
     * This determines the palette offset(0-3) to start reading colors from in the
     * sprite color palette memory area
     *
     * @param spriteSpot The sprite's position in the sprite ram
     * @return The value(from 0-3) of the sprite's beginning palette spot.
     */
    public int getSpritePalette(int spriteSpot) {
        int paletteStart = oam[spriteSpot + SPRITE_ATTR].getBits(SPRITE_PALETTE_1, SPRITE_PALETTE_0 + 1);
        return paletteStart;
    }

    /**
     * This returns the pattern table that the sprite reads its background image from.
     *
     * @param spriteSpot The sprite's position in sprite ram
     * @return The address to read the sprite's palette image from.
     */
    public int getPatternTableAddr(int spriteSpot) {
        int patternTableOffset = ppu.getPPUControl().getSpriteTableAddr();
        if (!ppu.getPPUControl().spritesNormal()) {//its 8x8
            patternTableOffset = (oam[spriteSpot * SPRITE_SIZE + SPRITE_ADDR].getBit(SPRITE_ADDR_BANK_BIT) == 0) ? PPU.PATTERN_SPRITE_ADDR : PPU.PATTERN_TILE_ADDR;
        }
        return patternTableOffset;
    }

    /**
     * This determines whether a sprite is flipped horizontally or not.
     *
     * @param spriteSpot The sprite's position in sprite ram
     * @return True if the sprite is flipped horizontally.
     */
    public boolean getSpriteFlippedHori(int spriteSpot) {
        return oam[spriteSpot + SPRITE_ATTR].getBit(SPRITE_FLIP_HORI) > 0;
    }

    /**
     * This determines whether a sprite is flipped vertically or not
     *
     * @param spriteSpot The sprite's position in sprite ram
     * @return True if the sprite is flipped vertically
     */
    public boolean getSpriteFlippedVert(int spriteSpot) {
        return oam[spriteSpot + SPRITE_ATTR].getBit(SPRITE_FLIP_VERT) > 0;
    }

    /**
     * This determines both the sprite's position and also its size, although the
     * size probably doesn't work.
     *
     * @param spriteSpot The sprite's position in sprite ram.
     * @return A rectangle representing the sprite's position and size.
     */
    public Rectangle getSpriteSize(int spriteSpot) {
        int x = oam[spriteSpot + SPRITE_X].getIntValue();
        int y = oam[spriteSpot + SPRITE_Y].getIntValue() + 1;
        int width = SPRITE_PX_X;
        int height = SPRITE_PX_Y;
        if (!ppu.getPPUControl().spritesNormal()) {
            height *= 2;
        }
        return new Rectangle(x, y, width, height);
    }

    /**
     * This returns the sprite's pattern table, based on the y(which should always
     * be 0 right now) and the sprite's position in ram
     *
     * @param y          Should just be 0, but with 8x16 sprites this changes
     * @param spriteSpot The sprite's position in ram
     * @return The byte value of the sprite's addr
     */
    public int getSpritePattern(int y, int spriteSpot) {
        return oam[spriteSpot + y / Tile.PIXELS_PER_TILE + SPRITE_ADDR].getIntValue();
    }
}
