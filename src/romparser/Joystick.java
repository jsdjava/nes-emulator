package romparser;

import java.awt.Canvas;
import java.awt.event.KeyEvent;
import nesengine.KeyMap;

public class Joystick extends AuxRegister {
    /**
     * The address of the first joystick register
     */
    public static final int BOT_REG_ADDR = 0x4016;
    /**
     * The address of the second joystick register, which is currently unimplemented
     */
    public static final int TOP_REG_ADDR = 0x4017;
    /**
     * This is used for timing of the NES reads of each bit,when its high the value of 
     * A is loaded, and the keys are allowed to change. When its low, each bit is returned 
     * in order.
     */
    public static final int STROBE_BIT = 7;
    /**
     * The bit corresponding to the a button on the controller
     */
    public static final int A_KEY = 0;
    /**
     * The bit corresponding to the b button on the controller
     */
    public static final int B_KEY = 1;
    /**
     * The bit corresponding to the select button on the controller
     */
    public static final int SELECT_KEY = 2;
    /**
     * The bit corresponding to the start button on the controller
     */
    public static final int START_KEY = 3;
    /**
     * The bit corresponding to the up axis on the controller
     */
    public static final int UP_KEY = 4;
    /**
     * The bit corresponding to the down axis on the controller
     */
    public static final int DOWN_KEY = 5;
    /**
     * The bit corresponding to the left axis on the controller
     */
    public static final int LEFT_KEY = 6;
    /**
     * The bit corresponding to the right axis on the controller
     */
    public static final int RIGHT_KEY = 7;
    /**
     * An object from another library that listens for keys, and hold their
     * current states in an array which is read into each of the NES bits.
     */
    private KeyMap keyMapper = new KeyMap();
    /**
     * This needs a canvas so it can register the keymapper on it, so key events on 
     * it can be listened to.
     * @param cnv The canvas that you want to register keys
     */
    public Joystick(Canvas cnv) {
        //Forces the canvas for the emulator to have this key listener added to it
        cnv.addKeyListener(keyMapper);
    }
    /**
     * Returns true if the current value is between the top and bottom registers.
     * @param value The value to check
     * @return True if value is a joystick register
     */
    public boolean isJoystickRegister(int value) {
        return BOT_REG_ADDR <= value && TOP_REG_ADDR >= value;
    }
    /**
     * Looks like this is used to determine if we should start strobing, based when only
     * the strobe bit is set to true. I think its like helping look for that 1- 0 pattern or 
     * something.
     */
    private int oldValue = 0;
    /**
     * This determines whether we should start handing out the value of each bit
     * in the register.
     */
    private boolean startStrobe = false;
    /**
     * The current bit to write out.
     */
    private int spot = 0;

    public void update() {
        if (used) {
            if (register.writtenTo()) {
                spot = 0;
                startStrobe = false;
                if (oldValue == 1 && register.getIntValue() == 0) {
                    startStrobe = true;
                }
                oldValue = register.getIntValue();
                register.setBit(STROBE_BIT, false);
                register.writtenTo();
            } 
            if (startStrobe) {
                if (spot < 8) {
                    register.setBit(STROBE_BIT, keyMapper.getKeyActive()[spot]);
                    register.writtenTo();
                    spot++;
                } else {
                    register.setBit(STROBE_BIT, false);
                    register.writtenTo();
                }
            }
            used = false;
        }
    }
    /**
     * This determines when the joystick register is written to.
     */
    private boolean used = false;

    /**
     * Returns the current value of the joysticks keys/strobe bit
     * @return The bytearray inside this(its basically just a bit, since it just strobes
     * out the value of each key.
     */
    @Override
    public ByteArray getRegister() {
        used = true;
        return super.getRegister();
    }
}
