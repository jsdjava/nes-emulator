package romparser;

public class APU {

    /**
     * This is the lowest address that correspond to a register in the APU
     */
    public static final int BOT_REG_ADDR = 0x4000;
    /**
     * This is the highest address that corresponds to a register in the APU
     */
    public static final int TOP_REG_ADDR = 0x4015;
    /**
     * This is the address of the oam register. When this is written to, sprites
     * are loaded from and address in ram to the ppu's internal sprite ram.
     */
    public static final int APU_OAM_REG = 0x4014;
    /**
     * The actual apu register object
     */
    private APUOam oamRegister = new APUOam();

    /**
     * This returns true if the address is inside the apu's address range.
     *
     * @param value the address
     * @return true if its between the bot and top reg addr's
     */
    public boolean isAPURegister(int value) {
        return BOT_REG_ADDR <= value && TOP_REG_ADDR >= value;
    }

    /**
     * Returns the value of a specific register mapped to the apu
     *
     * @param value The address of the register
     * @return Currently, it only returns an actual value if you pass in the apu oam register value
     */
    public ByteArray getAPURegister(int value) {
        switch (value) {
            case APU_OAM_REG:
                return oamRegister.getRegister();
        }
        return null;
    }

    /**
     * Determines if the ppu sprite ram needs to be loop loaded.
     *
     * @return True if the oam register has been changed.
     */
    public boolean callOAM() {
        boolean changed = oamRegister.isChanged();
        oamRegister.resetChanged();
        return changed;
    }
}
