package romparser;

public class PPUScroll extends AuxRegister {
    /**
     * This is the value to scroll in the x(I believe it range from 0 -255)
     */
    private int xScroll;
    /**
     * This would be used if vertical scrolling was enabled, which isn't written
     * right now, so its basically unused.
     */
    private int yScroll;
    /**
     * This determines whether the next write is to the value of xScroll, or the
     * value of yScroll
     */
    private int curSpot = 0;
    /**
     * This should get called every ppu cycle in order to make sure that
     * the curSpot value get incremented, and the register to write to next
     * is switched
     */
    public void update() {
        if (register.writtenTo()) {
            if (curSpot % 2 == 0) {
                xScroll = register.getIntValue();
            } else {
                yScroll = register.getIntValue();
            }
            curSpot++;
        }
    }
    /**
     * This should get called during every NMI, it forces curSpot back down to 0, meaning
     * that the next register to write to will be xScroll.
     */
    public void resetLatch(){
        curSpot = 0;
    }
    /**
     * This returns the value of the x scroll, in pixels
     * @return The value to shift all tiles by, in pixels, on the x axis
     */
    public int getScrollX() {
        return xScroll;
    }
    /**
     * Currently this isn't really used, but in vertical mirroring, it should
     * determine the value to shift all the tiles by in the y
     * @return The value to shift all tiles by, in pixels, on the y axis
     */
    public int getScrollY() {
        return yScroll;
    }
}
