package romparser;

public class PPUSpriteSpot extends AuxRegister {
    /**
     * This auto increments the spot, I believe it is used by loop load inside ppuoam
     */
    public void inc(){
        register.incValueWrap(1);
    }
}
