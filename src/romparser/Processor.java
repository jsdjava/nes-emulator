package romparser;

import java.math.BigInteger;
import java.awt.Canvas;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.UIManager;
import nesengine.graphic.Map;

public class Processor {

    /**
     * The address in memory where the instructions should begin(after ram and the stack). I don't think
     * this changes based on the mapper at all.
     */
    public static final int PRG_START = 0x8000;
    /**
     * The size of each instruction set block. I don't think that this changes with the mapper.
     */
    public static final int PRG_SIZE = 0x8000;
    /**
     * The address in memory where the stack starts(and then continues for STACK_SIZE (BYTE_SIZE) addresses.
     */
    public static final int STACK_START = 0x100;
    /**
     * The nes stack supports BYTE_SIZE addresses
     */
    public static final int STACK_SIZE = 0x100;
    /**
     * Just a constant used to clean up code, its the size of a byte(BYTE_SIZE)
     */
    public static final int BYTE_SIZE = 0x100;
    /**
     * When an nmi(something the ppu generates on frames) is called, this is the address in memory
     * where the address the program counter should jump to is stored
     */
    public static final int NMI_INTERRUPT_ADDR = 0xFFFA;
    /**
     * A flag in the status register that is set when there is a carry.
     */
    public static final int CARRY_FLAG = 7;
    /**
     * A flag in the status register that is set when the previous value was 0.
     */
    public static final int ZERO_FLAG = 6;
    /**
     * A flag in the status register that should be set when you don't want software
     * maskable interrupts(Note that this will not prevent an NMI)
     */
    public static final int INT_DISABLE_FLAG = 5;
    /**
     * A flag for using decimal numbers, should never be set because the nes's version
     * of the 6502 doesn't support decimal numbers.
     */
    public static final int DEC_MODE_FLAG = 4;
    /**
     * A flag for when an operation caused a register somewhere to overflow(usually adds and subtracts or rol and rors do this).
     */
    public static final int OVERFLOW_FLAG = 1;
    /**
     * This is set when a previous value was negative.
     */
    public static final int NEG_FLAG = 0;
    /**
     * Stores the address of the next instruction to execute. Modified by NMI's, and different
     * jmp commands.
     */
    private ByteArray programCounter = new ByteArray(0);
    /**
     * This is where most values are stored that are going to be used by the program a lot.
     * You can put memory addresses and stuff in here.Most commands use this(like LDA and STA)
     */
    private ByteArray accumulator = new ByteArray(0);
    /**
     * Similar to the accumulator, but i think it can also transfer its value
     * to th status register(TXS). Has its own addressing mode.
     */
    private ByteArray indexRegisterX = new ByteArray(0);
    /**
     * Also similar to the accumulator, but it has its own addressing mode.
     */
    private ByteArray indexRegisterY = new ByteArray(0);
    /**
     * Stores the states of the NEG_FLAG, DEC_FLAG, etc
     */
    private ByteArray statusRegister = new ByteArray(0x24);//Only 7,just ignore the last one(index 7)
    /**
     * Stores the program's current position in the stack(so where to push and pop from)
     */
    private ByteArray stackRegister = new ByteArray(0xFD);
    /**
     * Stores program instructions passed to it from the RomParser. Needs to be changed
     * to support different mappers.
     */
    private ByteArray[][] prgBlocks = new ByteArray[3][PRG_SIZE];
    /**
     * All the memory before the instructions, used as ram basically.
     */
    private ByteArray[] memory = new ByteArray[PRG_SIZE];
    /**
     * This will switch between instruction memory sets
     */
    private Mapper mapper = new Mapper();
    /**
     * Loaded from the RomParser, basically its all the different int values for the command
     * enums.
     */
    private HashMap opToCmd = new HashMap(); //Stores the string command
    /**
     * Same as opToCmd, except its all the int values for the Modes
     */
    private HashMap opToMode = new HashMap(); //Stores the mode per each opcode
    /**
     * This emulates the custom addon to the 6502 that made the NES so good at drawing
     * stuff to tv's.(Its basically a graphics chip).
     */
    private PPU ppu;
    /**
     * This is what handles sound in the NES. Not fully implemented yet.
     */
    private APU apu = new APU();
    /**
     * Emulates the NES controllers. Uses keypress to set a register that
     * the program then reads(just like a real controller).
     */
    private Joystick joystick;
    /**
     * Used for timing of ppu frames.
     */
    private int curCycles = 0;
    private ArrayList<String> nesTestLog;

    /**
     * AddressingMode is an enum that represents different places/ways of accessing memory
     * the processor should use when its running commands.
     */
    private enum AddressingMode {

        IMPLIED,
        ACCUMULATOR,
        IMMEDIATE,
        ZERO_PAGE,
        ZERO_PAGE_X,
        ZERO_PAGE_Y,
        RELATIVE,
        ABSOLUTE,
        ABSOLUTE_X,
        ABSOLUTE_Y,
        INDIRECT,
        INDIRECT_X,
        INDIRECT_Y;

        /**
         * Determines when an addressing mode uses more than one byte(so like absolute)
         *
         * @return True when the addressing mode is absolute, absolute_x, or absolute_y
         */
        public boolean isTwoByte() {
            return this == ABSOLUTE || this == ABSOLUTE_X || this == ABSOLUTE_Y;
        }

        /**
         * Determines how many processor cycles the current instruction uses up.
         *
         * @param highByteChanged Its like whether a change from one byte to two occurred(costs cycles)
         * @param isBranching     If a program command using the RELATIVE mode actually triggered
         * @return
         */
        public int getCycles(boolean highByteChanged, boolean isBranching) {
            switch (this) {
                case IMPLIED:
                    return 2;
                case IMMEDIATE:
                    return 2;
                case ZERO_PAGE:
                    return 3;
                case ZERO_PAGE_Y:
                    return 4;
                case ZERO_PAGE_X:
                    return 4;
                case ABSOLUTE:
                    return 4;
                case ABSOLUTE_X:
                    return 4 + (highByteChanged ? 1 : 0);
                case ABSOLUTE_Y:
                    return 4 + (highByteChanged ? 1 : 0);
                case INDIRECT:
                    return 5;
                case INDIRECT_X:
                    return 6;
                case INDIRECT_Y:
                    return 5 + (highByteChanged ? 1 : 0);
                case RELATIVE:
                    if (isBranching) {
                        if (highByteChanged) {
                            return 4;
                        }
                        return 3;
                    }
                    return 2;
            }
            assert true : "Fell through all addressing modes, which should be impossible";
            return Integer.MIN_VALUE;
        }
    }

    /**
     * Command is an enum that represents what command the processor should currently be running. See above links for
     * specific documentation on each of these.
     */
    private enum Command {

        ADC,
        AND,
        ASL,
        BCC,
        NCS,
        BEQ,
        BIT,
        BMI,
        BNE,
        BPL,
        BCS,
        BRK,
        BVC,
        BVS,
        CLS,
        CLC,
        CLD,
        CLI,
        CLV,
        CMP,
        CPX,
        CPY,
        DEC,
        DEX,
        DEY,
        EOR,
        INC,
        INX,
        INY,
        JMP,
        JSR,
        LDA,
        LDX,
        LDY,
        LSR,
        NOP,
        ORA,
        PHA,
        PHP,
        PLA,
        PLP,
        ROL,
        ROR,
        RTI,
        RTS,
        SBC,
        SEC,
        SED,
        SEI,
        STA,
        STX,
        STY,
        TAX,
        TAY,
        TSX,
        TXA,
        TXS,
        TYA,
        LAX,
        SAX,
        DCP,
        ISB,
        SLO,
        RLA,
        SRE,
        RRA;

        /**
         * Similar to the addressing mode stuff above, determines how many cycles a processor instruction costs
         *
         * @param addrMode        What addressing mode is being used
         * @param highByteChanged Whether a change from two bytes to one occurred in memory
         * @param isBranching     If an operation that causes branching actually branched
         * @return
         */
        public int getCycles(AddressingMode addrMode, boolean highByteChanged, boolean isBranching) {
            switch (this) {
                case INC: 
                case DEC:
                case LSR:
                case ROR:
                case ROL:
                case ASL:
                    switch (addrMode) {
                        case ACCUMULATOR:
                            return 2;
                        case ZERO_PAGE:
                            return 5;
                        case ZERO_PAGE_X:
                            return 6;
                        case ZERO_PAGE_Y:
                            return 6;
                        case ABSOLUTE_X:
                            return 7;
                    }
                //System.out.println("Fell through ASL, DEC, or INC on addressing modes");
                case BRK:
                    return 7;
                case JMP:
                    switch (addrMode) {
                        case ABSOLUTE:
                            return 3;
                        case INDIRECT:
                            return 5;
                    }
                    assert true : "Fell through all jmp addressing modes, which should be impossible";
                case RTI:
                case RTS:
                case JSR:
                    return 6;
                case PHP:
                case PHA:
                    return 3;
                case PLA:
                case PLP:
                    return 4;
                case STA:
                    switch (addrMode) {
                        case ZERO_PAGE:
                            return 3;
                        case ZERO_PAGE_X:
                            return 4;
                        case ABSOLUTE:
                            return 4;
                        case ABSOLUTE_X:
                            return 5;
                        case ABSOLUTE_Y:
                            return 5;
                        case INDIRECT_X:
                            return 6;
                        case INDIRECT_Y:
                            return 6;
                    }
                    assert true : "Fell through all store modes, which should be impossible";
                default:
                    return addrMode.getCycles(highByteChanged, isBranching);
            }
        }
    }
    public static Processor processor;
    public static ProcessorFrontEnd processorView;
    public static PPUFrontEnd ppuView = new PPUFrontEnd();

    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<ByteArray[]> instructionsList = RomParser.parseROM();
        ByteArray[] sprites = RomParser.getSprites();
        processorView = new ProcessorFrontEnd(instructionsList);
        ppuView.setVisible(true);
        processorView.setVisible(true);
        ByteArray[] instructions = new ByteArray[81920];
        int counter = 0;
        EXIT_INSTR_COPY:
        for (ByteArray[] curRow : instructionsList) {
            for (ByteArray curByte : curRow) {
                //System.out.println(curByte.getIntValue());
                instructions[counter] = curByte;
                counter++;
                if (counter >= instructions.length) {
                    break EXIT_INSTR_COPY;
                }
            }
        }
        counter--;
        boolean mirrorPrgBlocks = counter<=16384;
        for (ByteArray b : sprites) {
            instructions[counter] = b;
            counter++;
        }
        processor = new Processor(instructions, sprites, processorView.getEmulatorCanvas(),mirrorPrgBlocks);
    }

    /**
     * This is used to generate a new processor object for the frontend.
     *
     * @param nesCanvas The canvas that everything will be drawn on
     * @return A new processor instance.
     */
    public static Processor buildNESEnvironment(Canvas nesCanvas) {
        ArrayList<ByteArray[]> instructionsList = RomParser.parseROM();
        ByteArray[] sprites = RomParser.getSprites();
        ByteArray[] instructions = new ByteArray[81920];
        int counter = 0;
        EXIT_INSTR_COPY:
        for (ByteArray[] curRow : instructionsList) {
            for (ByteArray curByte : curRow) {
                //System.out.println(curByte.getIntValue());
                instructions[counter] = curByte;
                counter++;
                if (counter >= instructions.length) {
                    break EXIT_INSTR_COPY;
                }
            }
        }
        counter--;
        boolean mirrorPrgBlocks = counter<=16384;
        for (ByteArray b : sprites) {
            instructions[counter] = b;
            counter++;
        }
        return new Processor(instructions, sprites, nesCanvas,mirrorPrgBlocks);
    }

    /**
     * Constructs a new processor object.
     *
     * @param instructions The array of instructions read off from the rom.
     * @param sprites      Passed into the ppu to set its internal ram
     * @param renderCanvas Where everything will get drawn
     */
    public Processor(ByteArray[] instructions, ByteArray[] sprites, Canvas renderCanvas,boolean mirrorPrgBlocks) {
        //Initialize ppu
        ppu = new PPU(sprites, renderCanvas);
        //Initialize the joystick
        joystick = new Joystick(renderCanvas);
        //Initialize the arrays
        for (int j = 0; j < prgBlocks.length; j++) {
            prgBlocks[j] = new ByteArray[PRG_SIZE];
            ByteArray[] curProgBlock = prgBlocks[j];
            for (int i = 0; i < curProgBlock.length && j * curProgBlock.length + i < instructions.length; i++) {
                curProgBlock[i] = instructions[j * curProgBlock.length + i];
            }
        }
        for (int i = 0; i < memory.length; i++) {
            memory[i] = new ByteArray(0);
        }
        if(mirrorPrgBlocks){
          for(int i =0; i<16384; i++){
            if(prgBlocks[0][i+16384] == null){
                prgBlocks[0][i+16384] = new ByteArray(0);
            }
            prgBlocks[0][i+16384].setValue(prgBlocks[0][i].getIntValue());
          }
        }
        HashMap opToAsmStr = RomParser.getOpToAsm();
        HashMap opToModeInt = RomParser.getOpToMode();
        for (Object key : opToAsmStr.keySet()) {
            String value = (String) opToAsmStr.get(key);
            try {
                Command cmd = Command.valueOf(value);
                opToCmd.put(key, cmd);
            } catch (Exception e) {
            }
        }
        for (Object key : opToModeInt.keySet()) {
            String value = (String) opToModeInt.get(key);
            try {
                AddressingMode mode = AddressingMode.valueOf(value);
                opToMode.put(key, mode);
            } catch (Exception e) {
            }
        }
        //Stuff for processor initialization, just guessing here for now
        int lowByte = getAddr(0xff,0xfc).getIntValue();
        int highByte = getAddr(0xff, 0xfd).getIntValue();
        programCounter.setValue(lowByte+highByte*256);
        //updateProcessorView(processorView, ppuView);
    }
    //Debug stuff just delete this

    //public void printPalette() {
    //    ppu.printPalette();
    //}
    //public void printStack() {
    //    for (int i = STACK_START; i < STACK_START + BYTE_SIZE; i++) {
    //        if (memory[i].getIntValue() != 0) {
    //            System.out.println("Stack Position " + new BigInteger("" + i, 10).toString(16) + " = " + memory[i].getBigIntValue().toString(16));
    //        }
    //    }
    //}
    /**
     * This runs one instruction on the processor (based on the program counter).
     */
    public void nextInstruction() {
        int curSpot = programCounter.getIntValue();
        doInstruction(getProgAddr(curSpot).getIntValue(), getProgAddr(curSpot + 1).getIntValue(), getProgAddr(curSpot + 2).getIntValue());
    }

    /**
     * This runs 128 instructions on the processor(beginning at the program counter).
     */
    public void do128Instruction() {
        for (int i = 0; i < 128; i++) {
            //System.out.println(programCounter.getBigIntValue().toString(16));
            nextInstruction();
        }
    }

    /**
     * Does a specific number of processor instructions(beginning at the program counter)
     *
     * @param count How many instructions to do
     */
    public void doCountInstruction(int count) {
        for (int i = 0; i < count; i++) {
            nextInstruction();
        }
    }

    /**
     * Runs the processor until a specific condition inside this method returns true.
     * Used by me as a dumb breakpoint command.
     */
    public void doCustomInstruction() {
        //81d0
        //while (false) {
        //    nextInstruction();
        //}
    }

    /**
     * Runs the processor through PPU_FRAMES commands(basically its an entire frame)
     */
    public void doFrame() {
        while (curCycles < PPU.PPU_FRAMES) {
            nextInstruction();
        }
        curCycles = 0;
    }

    /**
     * Has the processor run an instruction based on two arguments and an opcode.
     *
     * @param opcode The value of the instruction
     * @param arg1   The first argument for the instruction
     * @param arg2   The second argument for the instruction
     */
    public void doInstruction(int opcode, int arg1, int arg2) { //arg2 and arg1 must be 8 chars long(because I concatenate them to make two byte numbers)
        AddressingMode addrMode = (AddressingMode) opToMode.get(opcode);
        if(addrMode == null){
            System.out.println("wat");
        }
        Command command = (Command) opToCmd.get(opcode);
        
        //C000  4C F5 C5  JMP $C5F5                       A:00 X:00 Y:00 P:24 SP:FD PPU:  0, 21 CYC:7
        String pcStr = new BigInteger("" + programCounter.getIntValue()).toString(16).toUpperCase();
        while(pcStr.length()<4){
          pcStr = '0'+pcStr;
        }
        String opCodeStr = new BigInteger("" + opcode).toString(16).toUpperCase();
        String arg1Str = new BigInteger("" + arg1).toString(16).toUpperCase();
        String arg2Str = new BigInteger("" + arg2).toString(16).toUpperCase();
        opCodeStr = opCodeStr.length() == 1 ? '0'+opCodeStr:opCodeStr;
        arg1Str = arg1Str.length() == 1 ? '0'+arg1Str:arg1Str;
        arg2Str = arg2Str.length() == 1 ? '0'+arg2Str:arg2Str;
        switch(addrMode){
          case IMPLIED:
          case ACCUMULATOR:
            arg1Str = "  ";
            arg2Str = "  ";
            break;
          case IMMEDIATE:
          case ZERO_PAGE:
          case ZERO_PAGE_X:
          case ZERO_PAGE_Y:
          case RELATIVE:
          case INDIRECT_X:
          case INDIRECT_Y:
            arg2Str = "  ";
            break;
          case INDIRECT:
        }
        //A:00 X:00 Y:00 P:24 SP:FD
        String accStr = new BigInteger(""+accumulator.getIntValue()).toString(16);
        String xStr = new BigInteger(""+indexRegisterX.getIntValue()).toString(16);
        String yStr = new BigInteger(""+indexRegisterY.getIntValue()).toString(16);
        String statusStr = new BigInteger(""+statusRegister.getIntValue()).toString(16);
        String spStr = new BigInteger(""+stackRegister.getIntValue()).toString(16);
        accStr = accStr.length()==1 ? '0'+accStr:accStr;
        xStr = xStr.length()==1 ? '0'+xStr:xStr;
        yStr = yStr.length()==1 ? '0'+yStr:yStr;
        statusStr = statusStr.length()==1 ? '0'+statusStr:statusStr;
        spStr = spStr.length()==1 ? '0'+spStr:spStr;
        String registerStr = "A:"+accStr+" X:"+xStr+" Y:"+yStr+" P:"+statusStr+" SP:"+spStr;
        String actualStr = (""+pcStr+"  "+opCodeStr+" "+arg1Str+" "+arg2Str+"  "+command.toString()+"                             "+registerStr).toUpperCase();
        //System.out.println(actualStr);
        /*String expectedStr = nesTestLog.get(nesTestLogIndex++);
        expectedStr = expectedStr.replace("*", " ");
        char[] expectedStrChars = expectedStr.toCharArray();
        for(int i = 0; i<28; i++){
          expectedStrChars[20+i] = ' ';
        }
        expectedStr = String.valueOf(expectedStrChars);
        expectedStr = expectedStr.split(" PPU:")[0];
        if(!actualStr.trim().equals(expectedStr)){
            System.out.println(actualStr+"\n"+expectedStr);
        }*/
        ByteArray value = null;
        boolean isTwoByteAddr = false;
        boolean highByteChanged = false;
        boolean branching = false;
        int address = 0;
        switch (addrMode) {//Determine what value we might be changing
            case IMPLIED:  //The command is specific to what its changing, it doesn't matter what value=
                value = new ByteArray(0);
                programCounter.incValue(1);
                break;
            case ACCUMULATOR://Set it equal to the accumulator object
                value = accumulator;
                programCounter.incValue(1);
                break;
            case IMMEDIATE://Its just a value that we're gonna use, based on arg 1(its only 8 byte)
                value = new ByteArray(arg1);
                programCounter.incValue(2);
                break;
            case ZERO_PAGE://Its an optimization, just means that we make the byteArray arg1 again - should be from ram
                value = getAddr(0, arg1);
                programCounter.incValue(2);
                break;
            case ZERO_PAGE_X: //Should grab the address in memory at registerx+arg1 SHOULD WRAP
                value = getAddr(0, indexRegisterX.addWithWrap(new ByteArray(arg1)).getIntValue());
                programCounter.incValue(2);
                break;
            case ZERO_PAGE_Y: //Same as x, except with the y register
                value = getAddr(0, indexRegisterY.addWithWrap(new ByteArray(arg1)).getIntValue());
                programCounter.incValue(2);
                break;
            case ABSOLUTE://Is the actual 16 bit in the processors memory that is passed in
                address =arg2 * BYTE_SIZE + arg1;
                value = getAddr(arg2, arg1);//Because its little endian, arg 2 and arg 1 get reversed
                programCounter.incValue(3);
                isTwoByteAddr = true;
                break;
            case ABSOLUTE_X://Takes the value from absolute, adds the value in the x register
                int highByte = arg2;
                ByteArray absXSpot = new ByteArray(arg2 * BYTE_SIZE + arg1 + indexRegisterX.getIntValue());
                if (absXSpot.getIntValue() / BYTE_SIZE != 0) {
                    highByteChanged = highByte != absXSpot.getBits(0, 8);
                }
                address =absXSpot.getIntValue();
                value = getAddr(address);
                programCounter.incValue(3);
                isTwoByteAddr = true;
                break;
            case ABSOLUTE_Y://Takes the value from absolute, adds the value in the y register
                highByte = arg2;
                ByteArray absYSpot = new ByteArray(arg2 * BYTE_SIZE + arg1 + indexRegisterY.getIntValue());
                if (absYSpot.getIntValue() / BYTE_SIZE != 0) {
                    highByteChanged = highByte != (absYSpot.getBits(0, 8));
                }
                address = absYSpot.getIntValue() & 0xFFFF;
                value = getAddr(address);
                //System.out.println("Absolute y is looking at " + absYSpot.getBigIntValue().toString(16));
                isTwoByteAddr = true;
                programCounter.incValue(3);
                break;
            case RELATIVE:
                value = new ByteArray(arg1);
                int startRelAddr = programCounter.getIntValue();
                highByte = 0;
                if (startRelAddr / BYTE_SIZE != 0) {
                    highByte = programCounter.getBits(0, 8);
                }
                ByteArray newAddr = new ByteArray(programCounter.getIntValue() + value.getSignedValue());
                if (newAddr.moreThan256() != programCounter.moreThan256() || highByte != newAddr.getBits(0, 8)) {
                    highByteChanged = true;
                }
                programCounter.incValue(2);
                break;
            case INDIRECT://Looks like its only used for the jump instruction, but it means that,
                //the value at (arg1 concat with arg2)+1 concat with it in memory 
                int spot1 = arg2 * BYTE_SIZE + arg1;
                int spot2 = arg2 * BYTE_SIZE + (arg1+1)%BYTE_SIZE;
                isTwoByteAddr = true;
                address = getAddr(spot2).getIntValue() * BYTE_SIZE + getAddr(spot1).getIntValue();
                //System.out.println("Program Counter "+programCounter.getBigIntValue().toString(16));
                //System.out.println("checking spot "+new BigInteger(""+spot).toString(16));
                //System.out.println("and also "+new BigInteger(""+spot+1).toString(16));
                ByteArray memPos = new ByteArray(address);
                //System.out.println("We're looking @ mem spot " + memPos.getBigIntValue().intValue());
                value = memPos;//Because its little endian
                programCounter.incValue(3);
                break;
            case INDIRECT_X://I think you take arg1 as the address, add the x register to it, (with wrap around) and then use the value at that address as the actual mem address
                spot1 = (arg1+indexRegisterX.getIntValue()+1)%256;
                spot2 = (arg1+indexRegisterX.getIntValue())%256;
                //System.out.println("spot is"+spot);
                isTwoByteAddr = true;
                address = new ByteArray(getAddr(spot1).getIntValue() * BYTE_SIZE + getAddr(spot2).getIntValue()).getIntValue();
                value = getAddr(address);
                //System.out.println("indirect x value is :"+value.getIntValue());
                programCounter.incValue(2);
                break;
            case INDIRECT_Y://Instead of adding x to the value at the address, you add it to arg2 concat with arg1
                spot1 = (arg1+1)%256;
                spot2 = arg1;
                int highMem = getAddr(spot1).getIntValue();
                int lowMem = getAddr(spot2).getIntValue();
                // dont know why it behaves this way, its not overflow its capped
                isTwoByteAddr =true;
                address = (highMem*256+lowMem+indexRegisterY.getIntValue()) & 0xffff;
                value = getAddr(address);
                programCounter.incValue(2);
                break;
            default:
                assert true : "Didn't recognize the addressing mode inside do instruction";
        }
        //Put ppu.is register and feed to internal switch case
        if (isTwoByteAddr && ppu.isPPURegister(address)) {
            //System.out.println("Accessing ppu register "+ppu.getMemorySpot().displayRegister().getIntValue());
            value = ppu.getPPURegister(arg2 * BYTE_SIZE + arg1, true);
            if(value == null){
                System.out.println("???");
            }
        } else if (isTwoByteAddr && apu.isAPURegister(arg2 * BYTE_SIZE + arg1)) {
            //System.out.println("Accessing an apu register " + Integer.parseInt(arg2 + arg1, 2));
            ByteArray possValue = apu.getAPURegister(arg2 * BYTE_SIZE + arg1);
            if (possValue != null) {
                value = possValue;
            }
        } else if (isTwoByteAddr && joystick.isJoystickRegister((arg2 * BYTE_SIZE + arg1))) {
            //System.out.println("Accessing a joystick register " + Integer.parseInt(arg2 + arg1, 2));
            value = joystick.getRegister();
        }
        assert command == null : "Didn't recognize the command provided, the opcode was " + opcode;
        switch (command) {//Change it
            case SEI:
                statusRegister.setBit(INT_DISABLE_FLAG, true);
                break;
            case SEC:
                statusRegister.setBit(CARRY_FLAG, true);
                break;
            case LDA:
                accumulator.setValue(value.getIntValue());
                setLoadFlags(accumulator);
                break;
            case STA:
                if (addrMode.isTwoByte() && mapper.isSwitching(arg2)) {
                    mapper.setBanks(accumulator);
                    //programCounter.setValue(PRG_START); //I'm guessing that everytime we switch banks, the prg counter should also be reset
                } else {
                    if(value ==null){
                        System.out.println("how");
                    }
                    value.setValue(accumulator.getIntValue());
                }
                break;
            case STX:
                value.setValue(indexRegisterX.getIntValue());
                break;
            case SAX:
                int saxValue = accumulator.getIntValue() & indexRegisterX.getIntValue();
                value.setValue(saxValue);
                break;
            case LAX:
                accumulator.setValue(value.getIntValue());
                setLoadFlags(accumulator);
                indexRegisterX.setValue(accumulator.getIntValue());
                setTransferFlags(indexRegisterX);
                break;
            case JMP:
                if (addrMode == AddressingMode.ABSOLUTE) {
                    value = new ByteArray(arg2 * BYTE_SIZE + arg1);
                }
                programCounter.setValue(value.getIntValue());
                break;
            case CLD:
                statusRegister.setBit(DEC_MODE_FLAG, false);
                break;
            case CLV:
                statusRegister.setBit(OVERFLOW_FLAG, false);
                break;
            case CLC:
                statusRegister.setBit(CARRY_FLAG, false);
                break;
            case LDX:
                indexRegisterX.setValue(value.getIntValue());
                setLoadFlags(indexRegisterX);
                break;
            case LDY:
                indexRegisterY.setValue(value.getIntValue());
                setLoadFlags(indexRegisterY);
                break;
            case TXS:
                stackRegister.setValue(indexRegisterX.getIntValue());
                break;
            case TSX:
                indexRegisterX.setValue(stackRegister.getIntValue());
                statusRegister.setBit(NEG_FLAG, indexRegisterX.getSignedValue()<0);
                statusRegister.setBit(ZERO_FLAG,indexRegisterX.getSignedValue()==0);
                break;         
            case BPL:
                if (statusRegister.getBit(NEG_FLAG) == 0) {
                    branching = true;
                    programCounter.setValue(programCounter.getIntValue() + value.getSignedValue());
                }
                break;
            case BVS:
                if (statusRegister.getBit(OVERFLOW_FLAG) == 1) {
                    branching = true;
                    programCounter.setValue(programCounter.getIntValue() + value.getSignedValue());
                }
                break;  
            case BVC:
                if (statusRegister.getBit(OVERFLOW_FLAG) == 0) {
                    branching = true;
                    programCounter.setValue(programCounter.getIntValue() + value.getSignedValue());
                }
                break;                
            case BMI:
                if (statusRegister.getBit(NEG_FLAG) == 1) {
                    branching = true;
                    programCounter.setValue(programCounter.getIntValue() + value.getSignedValue());
                }
                break;
            case BCS:
                if (statusRegister.getBit(CARRY_FLAG) == 1) {
                    branching = true;
                    programCounter.setValue(programCounter.getIntValue() + value.getSignedValue());
                }
                break;
            case ADC:
                ByteArray oldAccumulator = accumulator;
                accumulator = (accumulator.addWithOverflow(value).addWithOverflow(new ByteArray(statusRegister.getBit(CARRY_FLAG))));
                setAddFlags(oldAccumulator, value);
                break;
            case RRA:
                int lsb = value.getBit(7);
                value.setValue(value.rightRotate(statusRegister.getBit(CARRY_FLAG)).getIntValue());
                statusRegister.setBit(CARRY_FLAG, lsb > 0);
                setRotateFlags(value);
                oldAccumulator = accumulator;
                accumulator = (accumulator.addWithOverflow(value).addWithOverflow(new ByteArray(statusRegister.getBit(CARRY_FLAG))));
                setAddFlags(oldAccumulator, value);
                break;
            case SBC:
                oldAccumulator = accumulator;
                accumulator = accumulator.subWithOverflow(value).subWithOverflow(new ByteArray(statusRegister.getBit(CARRY_FLAG) > 0 ? 0 : 1));
                setSubFlags(accumulator, oldAccumulator, value);
                break;
            case ISB:
                value.incValueWrap(1);
                setIncFlags(value);
                oldAccumulator = accumulator;
                accumulator = accumulator.subWithOverflow(value).subWithOverflow(new ByteArray(statusRegister.getBit(CARRY_FLAG) > 0 ? 0 : 1));
                setSubFlags(accumulator, oldAccumulator, value);
                break;
            case INC:
                value.incValueWrap(1);
                setIncFlags(value);
                break;
            case INX:
                indexRegisterX.incValueWrap(1);
                setIncFlags(indexRegisterX);
                break;
            case INY:
                indexRegisterY.incValueWrap(1);
                setIncFlags(indexRegisterY);
                break;
            case DEC:
                value.incValueWrap(-1);
                setIncFlags(value);
                break;
            case DEX:
                indexRegisterX.incValueWrap(-1);
                setIncFlags(indexRegisterX);
                break;
            case SED:
                statusRegister.setBit(DEC_MODE_FLAG, true);
                break;
            case BIT:
                ByteArray andOp = accumulator.and(value);
                setBitFlags(andOp,value);
                break;
            case CMP:
                setCmpFlags(accumulator, value);
                break;
            case DCP:
                value.incValueWrap(-1);
                setIncFlags(value);
                setCmpFlags(accumulator, value);
                break;
            case CPY:
                setCmpFlags(indexRegisterY, value);
                break;
            case CPX:
                setCmpFlags(indexRegisterX, value);
                break;
            case BCC:
                if (statusRegister.getBit(CARRY_FLAG) == 0) {
                    branching = true;
                    programCounter.setValue(programCounter.getIntValue() + value.getSignedValue());
                }
                break;
            case PLA:
                stackRegister.incValue(1);
                accumulator.setValue(memory[stackRegister.getIntValue() + STACK_START].getIntValue());
                setPullFlags(accumulator);
                break;
            case BEQ:
                if (statusRegister.getBit(ZERO_FLAG) == 1) {
                    branching = true;
                    programCounter.incValue(value.getSignedValue());
                }
                break;
            case DEY:
                indexRegisterY.incValueWrap(-1);
                setDecFlags(indexRegisterY);
                break;
            case BNE:
                if (statusRegister.getBit(ZERO_FLAG) == 0) {
                    branching = true;
                    programCounter.incValue(value.getSignedValue());
                }
                break;
            case JSR:
                ByteArray curProgCounter = new ByteArray(programCounter.getIntValue() - 1);
                memory[stackRegister.getIntValue() + STACK_START].setValue(curProgCounter.getBits(0, 8));
                stackRegister.incValue(-1);
                memory[stackRegister.getIntValue() + STACK_START].setValue(curProgCounter.getBits(8, 16));
                stackRegister.incValue(-1);
                programCounter.setValue(arg2 * BYTE_SIZE + arg1);
                break;
            case ORA:
                accumulator = accumulator.or(value);
                setLogicFlags(accumulator);
                break;
            case SLO:
                statusRegister.setBit(CARRY_FLAG, value.getBit(0) != 0);
                value.setValue(value.leftShift().getIntValue());
                setShiftFlags(value);
                accumulator = accumulator.or(value);
                setLogicFlags(accumulator);
                break;
            case EOR:
                accumulator = accumulator.xor(value);
                setLogicFlags(accumulator);
                break;
            case SRE:
                statusRegister.setBit(CARRY_FLAG, value.getBit(7) != 0);
                value.setValue(value.rightShift().getIntValue());
                setShiftFlags(value);
                accumulator = accumulator.xor(value);
                setLogicFlags(accumulator);
                break;
            case AND:
                accumulator = accumulator.and(value);
                setLogicFlags(accumulator);
                break;
            case RLA:
                int msb = value.getBit(0);
                value.setValue(value.leftRotate(statusRegister.getBit(CARRY_FLAG)).getIntValue());
                statusRegister.setBit(CARRY_FLAG, msb > 0);
                setRotateFlags(value);
                accumulator = accumulator.and(value);
                setLogicFlags(accumulator);
                break;
            case PHP:
                int statusValue = statusRegister.getIntValue();
                memory[stackRegister.getIntValue() + STACK_START].setValue(statusValue);
                memory[stackRegister.getIntValue() + STACK_START].setBit(2,true);
                memory[stackRegister.getIntValue() + STACK_START].setBit(3,true);
                stackRegister.incValue(-1);
                break;
            case PHA:
                memory[stackRegister.getIntValue() + STACK_START].setValue(accumulator.getIntValue());
                stackRegister.incValue(-1);
                break;
            case PLP:
                stackRegister.incValue(1);
                boolean bit3 = statusRegister.getBit(3)>0;
                boolean bit2 = statusRegister.getBit(2)>0;
                statusRegister.setValue(memory[stackRegister.getIntValue() + STACK_START].getIntValue());
                statusRegister.setBit(3, bit3);
                statusRegister.setBit(2, bit2);
                break;
            case RTI:
                bit3 = statusRegister.getBit(3)>0;
                bit2 = statusRegister.getBit(2)>0;
                //System.out.println("Technically this method is incorrect because it should set the lower and upper ones of the programcounter in their own spots on the stack");
                stackRegister.incValue(1);
                statusRegister.setValue(memory[stackRegister.getIntValue() + STACK_START].getIntValue());
                stackRegister.incValue(1);
                int lowerPos = memory[stackRegister.getIntValue() + STACK_START].getIntValue();
                stackRegister.incValue(1);
                int upperPos = memory[stackRegister.getIntValue() + STACK_START].getIntValue();
                programCounter.setValue(new ByteArray(upperPos * BYTE_SIZE + lowerPos).getIntValue());
                statusRegister.setBit(3, bit3);
                statusRegister.setBit(2, bit2);
                break;
            case RTS:
                stackRegister.incValue(1);
                lowerPos = memory[stackRegister.getIntValue() + STACK_START].getIntValue();
                stackRegister.incValue(1);
                upperPos = memory[stackRegister.getIntValue() + STACK_START].getIntValue();
                programCounter.setValue(new ByteArray(upperPos * BYTE_SIZE + lowerPos).getIntValue() + 1);
                break;
            case TXA:
                accumulator.setValue(indexRegisterX.getIntValue());
                setTransferFlags(accumulator);
                break;
            case TYA:
                accumulator.setValue(indexRegisterY.getIntValue());
                setTransferFlags(accumulator);
                break;
            case TAY:
                indexRegisterY.setValue(accumulator.getIntValue());
                setTransferFlags(indexRegisterY);
                break;
            case TAX:
                indexRegisterX.setValue(accumulator.getIntValue());
                setTransferFlags(indexRegisterX);
                break;
            case STY:
                value.setValue(indexRegisterY.getIntValue());
                break;
            case ASL:
                //System.out.println("addr " + addrMode.name());
                statusRegister.setBit(CARRY_FLAG, value.getBit(0) != 0);
                value.setValue(value.leftShift().getIntValue());
                setShiftFlags(value);
                break;
            case ROR:
                lsb = value.getBit(7);
                value.setValue(value.rightRotate(statusRegister.getBit(CARRY_FLAG)).getIntValue());
                statusRegister.setBit(CARRY_FLAG, lsb > 0);
                setRotateFlags(value);
                break;
            case ROL:
                msb = value.getBit(0);
                value.setValue(value.leftRotate(statusRegister.getBit(CARRY_FLAG)).getIntValue());
                statusRegister.setBit(CARRY_FLAG, msb > 0);
                setRotateFlags(value);
                break;
            case LSR:
                //System.out.println("addr " + addrMode.name());
                statusRegister.setBit(CARRY_FLAG, value.getBit(7) != 0);
                value.setValue(value.rightShift().getIntValue());
                setShiftFlags(value);
                break;
            default:
                assert true: command.name() + " using addressing mode " + addrMode.name()+"has not been implemented";
        }
        joystick.update();
        boolean callOAM = apu.callOAM();
        if (callOAM) {
            int offset = accumulator.getIntValue() * PPUOam.OFFSET_MULT;
            //System.out.println("Loading ppu oam "+offset);
            for (int i = 0; i < PPUOam.BYTE_SIZE; i++) {
                ppu.getPPUOam().loopLoad(getAddr(offset + i));
            }
            ppu.getPPUOam().resetLoad();
        }
        int cycleInc = command.getCycles(addrMode, highByteChanged, branching);
        curCycles += cycleInc;
        boolean callNMI = false;
        for (int i = 0; i < 3 * cycleInc && !callNMI; i++) {
            ppu.update();
            callNMI = ppu.callNMI();
        }
        if (callNMI) { //Forgot to do stuff with the stack that you apparently need...
            //System.out.println("Technically this method is incorrect because it should set the lower and upper ones of the programcounter in their own spots on the stack");
            //Pushes the current program counter onto the stack 
            ByteArray curProgCounter = new ByteArray(programCounter.getIntValue());
            memory[stackRegister.getIntValue() + STACK_START].setValue(curProgCounter.getBits(0, 8));
            stackRegister.incValue(-1);
            memory[stackRegister.getIntValue() + STACK_START].setValue(curProgCounter.getBits(8, 16));
            stackRegister.incValue(-1);
            //Pushes the processor status onto the stack
            memory[stackRegister.getIntValue() + STACK_START].setValue(statusRegister.getIntValue());
            stackRegister.incValue(-1);
            //Set the interrupt disable flag to true
            statusRegister.setBit(INT_DISABLE_FLAG, true);
            programCounter.setValue(getProgAddr(NMI_INTERRUPT_ADDR + 1).getIntValue() * BYTE_SIZE + getProgAddr(NMI_INTERRUPT_ADDR).getIntValue());
        }
    }

    /**
     * Returns the value of a memory address from the memory arrays.
     *
     * @param arg1 The lower byte position
     * @param arg2 The higher byte position
     * @return A ByteArray at a specific position in memory.
     */
    public ByteArray getAddr(int arg1, int arg2) {       //Hardcoded nes 66 mapper , expects it big endian
        int addrVal = arg1 * BYTE_SIZE + arg2;
        return getAddr(addrVal);
    }

    /**
     * Returns the value of a memory address
     *
     * @param val The lower and higher byte combined
     * @return The ByteArray at the specific position in memory.
     */
    public ByteArray getAddr(int val) {
        if (val >= memory.length) {
            int index = val - PRG_START;
            return prgBlocks[mapper.getPrgBank()][index];
        }
        return memory[val];
    }

    /**
     * Gets a value inside the instructions (not ram)
     *
     * @param val the position inside the instructions to access
     * @return The ByteArray inside the instruction, or in memory if val is too small.
     */
    public ByteArray getProgAddr(int val) {
        if (val >= PRG_START) {
            int index = (val - PRG_START)%32768;
            ByteArray instruction = prgBlocks[mapper.getPrgBank()][index];
            return instruction;
        } else {
            return memory[val];//Holy you can jump to a place in ram i didnt even
        }
    }

    /**
     * Used by various load commands set the Zero and Neg flag.
     *
     * @param The value to use to determine what the flags should get set to
     */
    private void setLoadFlags(ByteArray value1) { //Automates some of the obvious flag setting values, it goes CARRY,ZERO,DISABLE INTERRUPT, DEC MODE,BREAKS,OVERFLOW,NEGATIVE
        statusRegister.setBit(ZERO_FLAG, value1.getIntValue() == 0);
        statusRegister.setBit(NEG_FLAG, value1.getBit(0) == 1);
    }

    /**
     * Used by the add command to set the overflow, zero, carry, and neg flags
     *
     * @param value1 The original value
     * @param value2 The changed value
     */
    private void setAddFlags(ByteArray value1, ByteArray value2) {
        int unsignedSum = value1.getSignedValue()+value2.getSignedValue()+statusRegister.getBit(CARRY_FLAG);
        statusRegister.setBit(OVERFLOW_FLAG, unsignedSum<-128 || unsignedSum >127);
        statusRegister.setBit(ZERO_FLAG, (value1.addWithOverflow(value2).addWithOverflow(new ByteArray(statusRegister.getBit(CARRY_FLAG)))).getIntValue() == 0);
        statusRegister.setBit(CARRY_FLAG, (value1.getIntValue() + value2.getIntValue() + new ByteArray(statusRegister.getBit(CARRY_FLAG)).getIntValue() > 255));
        statusRegister.setBit(NEG_FLAG, accumulator.getBit(0) == 1);
        //statusRegister.setBit(CARRY_FLAG, value1.getIntValue() + value2.getIntValue() > 255);
    }

    /**
     * Used by the sub command to set the overflow, carry, neg, and zero flags
     *
     * @param answer         The final result
     * @param oldAccumulator The old value in the accumulator
     * @param oldMemory      The old value in the memory address
     */
    private void setSubFlags(ByteArray answer, ByteArray oldAccumulator, ByteArray oldMemory) {
        int unSignedOp = oldAccumulator.getIntValue() - oldMemory.getIntValue() - (statusRegister.getBit(CARRY_FLAG) > 0 ? 0 : 1);
        int signedResult = oldAccumulator.getSignedValue()-oldMemory.getSignedValue()-(statusRegister.getBit(CARRY_FLAG) > 0 ? 0 : 1);
        statusRegister.setBit(OVERFLOW_FLAG, signedResult<-128 || signedResult >127);
        statusRegister.setBit(CARRY_FLAG, unSignedOp >= 0);
        statusRegister.setBit(NEG_FLAG, answer.getBit(0) > 0);
        statusRegister.setBit(ZERO_FLAG, answer.getIntValue() == 0);
    }

    /**
     * Used by different bitwise commands to set the neg, overflow, and zero flags
     *
     * @param andOp The value that has been modified by the command
     */
    public void setBitFlags(ByteArray andOp,ByteArray val) {
        statusRegister.setBit(ZERO_FLAG, andOp.getIntValue() == 0);
        statusRegister.setBit(NEG_FLAG, val.getBit(0) > 0);
        statusRegister.setBit(OVERFLOW_FLAG, val.getBit(1) > 0);
    }

    /**
     * Used by different compare flags to set the neg, carry, and zero flag
     *
     * @param value1 The first compared value
     * @param value2 The value its compared to
     */
    public void setCmpFlags(ByteArray value1, ByteArray value2) {
        int subResult = value1.subWithOverflow(value2).getSignedValue();
        statusRegister.setBit(NEG_FLAG, subResult < 0);
        statusRegister.setBit(CARRY_FLAG, value1.getIntValue() >= value2.getIntValue());
        statusRegister.setBit(ZERO_FLAG, value1.getIntValue() == value2.getIntValue());
    }

    /**
     * Used by the pull command
     *
     * @param value1 The value that has been loaded/pulled
     */
    public void setPullFlags(ByteArray value1) {
        setLoadFlags(value1);
    }

    /**
     * Used to set the zero and neg flags
     *
     * @param value1 The value that has been decremented
     */
    public void setDecFlags(ByteArray value1) {
        setLoadFlags(value1);
    }

    /**
     * Used to set the zero and neg flags
     *
     * @param value1 The value that has been incremented
     */
    public void setIncFlags(ByteArray value1) {
        setLoadFlags(value1);
    }

    /**
     * Used by the and, xor command(i think) to set the zero and neg flags
     *
     * @param value1 The value that has been modified the and or xor commands
     */
    public void setLogicFlags(ByteArray value1) {
        setLoadFlags(value1);
    }

    /**
     * Used by shifting commands to set the zero and neg flags
     *
     * @param value1 The value modified by the shifting command
     */
    public void setShiftFlags(ByteArray value1) {
        setLoadFlags(value1);
    }

    /**
     * Used by the rotate commands to set the zero and neg flags
     *
     * @param value1 The value modified by rotating commands
     */
    public void setRotateFlags(ByteArray value1) {
        setLoadFlags(value1);
    }

    /**
     * Used by the transferring commands(like txs)
     *
     * @param value1 The value that has been transferred
     */
    public void setTransferFlags(ByteArray value1) {
        setLoadFlags(value1);
    }

    /**
     * Not really maintained currently, but it loads the sprite map
     * so it can be displayed in the debugger
     *
     * @return A map of all sprites
     */
    public Map loadSpriteMap() {
        return ppu.getSpriteNameTableMap();
    }

    /**
     * Loads the background tiles so they can be displayed in the debugger.
     *
     * @return A map of all background tiles
     */
    public Map loadBackMap() {
        return ppu.getTileNameTableMap();
    }

    /**
     * Also only really for the debugger, but it returns all the current
     * values in zero page ram
     *
     * @return
     */
    public ByteArray[] getZeroPageRAM() {
        ByteArray[] zeroPageRam = new ByteArray[512];
        for (int i = 512; i < 512 * 2; i++) {
            zeroPageRam[i - 512] = memory[i];
        }
        return zeroPageRam;
    }

    /**
     * Loads what the current screen should look like(i think it ignores scrolling though)
     *
     * @return A map of the current screen
     */
    public Map loadCurrentMap() {
        return ppu.getMap();
    }

    /**
     * Used by the debugger to set different labels and table values on the processor frame
     *
     * @param processorView The processor view to update
     * @param ppuView       The nes view to update
     */
    public void updateProcessorView(ProcessorFrontEnd processorView, PPUFrontEnd ppuView) {
        processorView.setAccumulator(accumulator);
        processorView.setRegisterTable(statusRegister);
        processorView.setIndexRegisterXLabel(indexRegisterX);

        processorView.setIndexRegisterYLabel(indexRegisterY);
        processorView.setProgramCounterLabel(programCounter);
        processorView.setStackRegisterLabel(stackRegister);
        processorView.setProgramBankLabel(mapper.getPrgBank());
        processorView.setCyclesLabel(curCycles);
        ppuView.setControlLabel(ppu.getPPURegister(PPU.PPU_CONTROL_REG, false));
        ppuView.setMaskLabel(ppu.getPPURegister(PPU.PPU_MASK_REG, false));
        ppuView.setStatusLabel(ppu.getPPURegister(PPU.PPU_STATUS_REG, false));
    }
}
