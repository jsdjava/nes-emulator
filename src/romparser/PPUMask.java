package romparser;

public class PPUMask extends AuxRegister{
    /**
     * Determines whether to render to the screen during the current ppu cycle
     */
    private static final int MASK_RENDER_BIT = 4;
    /**
     * Tells the ppu whether tiles should get rendered not. Currently, sprites aren't
     * rendered quite like they should be, so I don't think this would be very effective
     * for them.
     * @return 
     */
    public boolean isRendering(){
        return register.getBit(MASK_RENDER_BIT)>0;
    }
}
