package romparser;

public class Mapper {
    /**
     * The current program bank to access(there are only supposed to be 2, this allows for 3,
     * and determines which one to that the processor should access.
     */
    private int curPrgBank;
    
    /**
     * This determines what the new memory bank should be, based on the value
     * of bits 2 and 4 (with MSB being 0)
     * @param arg2 The value that is being written out to the address that has been 
     * identified as a switcher.
     */
    public void setBanks(ByteArray arg2){ //xxPPxxCC
        curPrgBank = arg2.getBits(2,4);
    //    String cc = arg2.getStringValue().substring(6);
    //    curChrBank = Byte.parseByte(cc,2);
    }
    /**
     * Determines if the address being written to in the processor is supposed to
     * change memory banks.
     * @param arg1 The address being written to
     * @return True if a switch should be performed
     */
    public boolean isSwitching(int arg1){
        return (arg1>=128);
    }
    /**
     * The value of the current program memory banke
     * @return A value from 0 - 3 that determines what memory bank to read instructions from.
     */
    public int getPrgBank(){
        return curPrgBank;
    }
}
