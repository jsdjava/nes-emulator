package romparser;

public class PPUControl extends AuxRegister {

    /**
     * The LSB of the two bit number that determines one of four different nametables
     * that we are reading from(vertical vs. horizontal mirroring)
     */
    public static final int BASE_TABLE_ADDR_1 = 7; //Selects with the next one 
    /**
     * The MSB of the two bit number that determines one of four different nametables
     * for us to read from
     */
    public static final int BASE_TABLE_ADDR_2 = 6; //between 4 different addrs
    /**
     * The amount to increment the current memory spot by(either 1 or 32)
     */
    public static final int VRAM_ADDR_INC = 5;
    /**
     * Which sprite table to use (0 or 1) - Not really used right now,
     */
    public static final int SPRITE_TABLE_ADDR = 4;
    /**
     * Which tile table to use(0 or1)
     * Also not really used right now
     */
    public static final int BACK_TABLE_ADDR = 3;
    /**
     * Determines whether sprites are 8 or 16 pixels tall, isn't used right now
     */
    public static final int SPRITE_SIZE = 2;
    /**
     * Pretty much unimplemented in the nes, but its supposed to determine whether this ppu
     * is in master or slave mode.
     */
    public static final int PPU_MS_SELECT = 1;
    /**
     * Determines whether the processor should be calling an NMI
     */
    public static final int NMI_AT_VBI = 0;

    /**
     * This determines which of the four nametables we should be reading from.
     * Currently, I think only vertical mirroring works, so this may not be entirely
     * used right.
     *
     * @return A value from 8192 to 8192+1024*3
     */
    public int getNameTableAddr() {
        int offset = PPU.NAME_SIZE + PPU.NAME_COLOR_SIZE;
        int startSpot = PPU.NAME_1_ADDR;
        switch ("" + register.getBit(BASE_TABLE_ADDR_2) + register.getBit(BASE_TABLE_ADDR_1)) {
            case "00":
                return startSpot;
            case "01":
                return startSpot + offset;
            case "10":
                return startSpot + offset * 2;
            case "11":
                return startSpot + offset * 3;
        }
        return -Integer.MAX_VALUE; //This is bad and should never happen
    }

    /**
     * This returns the lsb name table (so basically the vertical scroll i think)
     *
     * @return 1 or 0, determining whether the ppu should start at 0 or 256px in the x
     */
    public int getNameTableSpot() {
        return register.getBit(BASE_TABLE_ADDR_1);
    }

    /**
     * Currenty not really used, but it checks whether sprites are 8px or 16px
     *
     * @return true if 8px(normal) or false if 16px
     */
    public boolean spritesNormal() {
        return register.getBit(SPRITE_SIZE) == 0;
    }
    /**
     *  This determines which table to use to read the current sprite palette tile from
     * @return Which pattern table, tile or sprite, to use
     */
    public int getSpriteTableAddr() {
        return (register.getBit(SPRITE_TABLE_ADDR) == 0) ? PPU.PATTERN_SPRITE_ADDR : PPU.PATTERN_TILE_ADDR;
    }
}
