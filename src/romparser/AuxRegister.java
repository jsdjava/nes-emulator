package romparser;

public class AuxRegister {
    /**
     * Holds the value of the register object
     */
    protected ByteArray register = new ByteArray(0);
    /**
     * Returns the register, but usually also triggers a flag
     * @return register
     */
    public ByteArray getRegister(){
        return register;
    }
    /**
     * That flag won't be triggered here, because its only being accessed for
     * display purposes, not actual processor logic.
     * @return register
     */
    public ByteArray displayRegister(){ //This is intentional, call this for updating registers on the display in the ppu.
        return register;
    }
}
