package romparser;

public class PPUMemoryWrite extends AuxRegister {
    /**
     * An internal reference to the ppu
     */
    private PPU ppu;
    /**
     * The memory address of the spot in ppu ram to write to
     */
    private int spot = -1;
    /**
     * The constructor for a ppu memory write object
     * @param ppu A reference to the current ppu
     */
    public PPUMemoryWrite(PPU ppu) {
        this.ppu = ppu;
    }
    /**
     * Gets the ByteArray inside ppu memory at the spot held in ppu memory spot
     * @return The ByteArray inside ppu ram that is pointed to by ppu memory spot
     */
    @Override
    public ByteArray getRegister() {
        PPUMemorySpot memSpot = ppu.getMemorySpot();
        spot = memSpot.getMemorySpot().getIntValue();
        return ppu.getMemory(spot);
    }
    /**
     * This tells the ppu what address in memory was changed
     * @return The memory address in ppu ram that was changed
     */
    public int getChangedSpot(){
        int copySpot = spot;
        spot = -1;
        return copySpot;
    }
    
}
