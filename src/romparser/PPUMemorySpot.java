package romparser;

public class PPUMemorySpot extends AuxRegister {
    /**
     * The upper 6 bits of the memory address in the ppu that should be read/written
     * to next.
     */
    protected ByteArray upper = new ByteArray(0);
    /**
     * Determines whether to set the upper or lower memory address of the current spot
     * that the ppu will be reading and writing from
     */
    private int upperOrLower = 1;
    /**
     * An internal reference to the ppu that helps check what value to increment
     * the memory spot every time this is read from.
     */
    private PPU ppu;
   /**
    * This constructs a ppu memory spot register.
    * @param ppu A reference to the ppu
    */ 
    public PPUMemorySpot(PPU ppu){
        this.ppu = ppu;
    }
    /**
     * Returns either the upper or lower memory address registers, depending on the
     * state of upperOrLower
     * @return Upper or lower register, depending on upperOrLower
     */
    @Override
    public ByteArray getRegister() {
        switch (upperOrLower) {
            case 0:
                upperOrLower++;
                return super.getRegister();
            case 1:
                upperOrLower = 0;
                return upper;
        }
        return null;
    }
    
    /**
     * This gets the next address to read/write from inside the ppu's ram.
     * @return The ppu memory address to read or write from
     */
    public ByteArray getMemorySpot() {
        return new ByteArray(upper.getBits(2,8)*256 + register.getIntValue());
    }
    
    /**
     * This will return the memory spot, but it won't cause the spot to be incremented.
     * Useful for gui stuff, just displaying the value of this.
     * @return The memory spot that this currently holds
     */
    @Override
    public ByteArray displayRegister(){
        return getMemorySpot();
    }
    /**
     * This should be called at the end of every ppu frame inside nmi.
     * It resets the upperOrLower register switch, as well as the firstRead
     * switch
     */
    public void resetLatch(){
        upperOrLower = 1;
        firstRead = false;
    }
    /**
     * Determines whether this is the first read this frame(because then
     * this isn't supposed to auto increment the memory address)
     */
    private boolean firstRead = false;
    /**
     * This increments the memory address of the current ppu spot by 1
     */
    public void incReadSpot(){
        if(!firstRead){
            firstRead = true;
        }
        else{
            incSpot(+1);
        }
    }
    /**
     * This incrememnts the memory address of the current ppu spot by either 1 or
     * 32 depending on the switch inside the ppu control register
     */
    public void incWriteSpot() {
        int inc = 1;
        if (ppu.getPPUControl().getRegister().getBit(PPUControl.VRAM_ADDR_INC) == 1) {
            inc = 32;
        }
        incSpot(inc);
    }
    /**
     * Actually increments the internal values of each register inside ppu memory spot
     * @param x The value to increment it by (should only be 1 or 32)
     */
    private void incSpot(int x){
        ByteArray wholeValue = new ByteArray(upper.getIntValue()*256+super.getRegister().getIntValue());
        wholeValue.incValue(x);
        if(wholeValue.getIntValue()>255){
            upper = new ByteArray(wholeValue.getBits(0,8));
            register = new ByteArray(wholeValue.getBits(8,16));
        }
        else{
            upper = new ByteArray(0);
            register = wholeValue; 
        }
        
    }
}
