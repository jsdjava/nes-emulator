# Nes Emulator
## Demo

[![Thumbnail](pics/thumbnail.gif)](https://drive.google.com/file/d/15_rTiec1Kdyf5MHjoo45QrA4d3mEw0vD/view?usp=sharing "Assembly Pong - Click to open video")
[Click image (or here) to open video](https://drive.google.com/file/d/15_rTiec1Kdyf5MHjoo45QrA4d3mEw0vD/view?usp=sharing)

## Description
Nes Emulator is a project I wrote with a friend in college that emulates the NES (6502 processor + PPU).
The code is extremely bad and buggy but I think it was a cool idea and I learned a lot about assembly from
writing it. I have definitely learned to structure projects better (as well as to write
tests when possible) as part of creating this.

Major features completed are:
* Full 6502 processor implementation (passes nestest rom)
* PPU (very buggy, does not work right for a lot of games)
* Can play some (not many) NES games
* Debugger tool for 6502

## Running Locally
The project can be loaded as a java project in Vscode, and run from Frontend.java.
Make sure to set the path to your nest ROM in RomParser.java.

## Attribution
* falling.nes - https://github.com/xram64/falling-nes
* alter_ego.nes - https://shiru.untergrund.net/software.shtml
